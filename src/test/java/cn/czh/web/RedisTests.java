package cn.czh.web;

import cn.czh.web.service.RedisService;
import cn.czh.web.util.LoginUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.TimeUnit;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisTests {

    @Autowired
    private RedisService service;

    @Test
    public void test1() {
//        RedisService.getInstance().set("key" , "value");
//        redisTemplate.opsForValue().set("key","value");

        service.set("key", "value");
        System.out.println(service.get("key"));
    }

    @Test
    public void test2() {
        service.set("cookie:keytime","value",30,TimeUnit.MINUTES);
        System.out.println(service.get("cookie:keytime"));
    }

    @Test
    public void test3(){

    }
}
