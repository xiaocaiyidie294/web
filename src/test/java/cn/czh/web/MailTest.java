package cn.czh.web;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MailTest {

    @Autowired
    private JavaMailSender mailSender;

    @Test
    public void sendEmail(){
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("773527141@qq.com");
        message.setTo("773527141@qq.com");
        message.setSubject("邮件测试");
        message.setText("测试邮件内容");

        mailSender.send(message);
    }

}
