package cn.czh.web;

import cn.czh.web.service.RedisService;
import cn.czh.web.util.AESUtil;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StringTests {

    private int TIMES = 1000;

    @Autowired
    RedisService redisService;

    @Test
    public void test1() {
        for (int i = 0; i < TIMES; i++) {
            String s1 = "token1" + ":" + "w8iJhc3uDfinr%2BEv37VPuMqByQkuQm6z2zo88pLaiJxy7y2j%2BpAMHF6juh7jzXcM%2F6v7ND7tZwPweV9R3T1CBEN5j%2FBEnfkOsF4EQ7KeiAc%3D";
            String s2 = "token2" + ":" + "57077a4adfce0c4c3198e1d0dbacfd7f";
        }
    }

    @Test
    public void test2() {
        for (int i = 0; i < TIMES; i++) {
            String s1 = "token1".concat(":").concat("w8iJhc3uDfinr%2BEv37VPuMqByQkuQm6z2zo88pLaiJxy7y2j%2BpAMHF6juh7jzXcM%2F6v7ND7tZwPweV9R3T1CBEN5j%2FBEnfkOsF4EQ7KeiAc%3D");
            String s2 = "token2".concat(":").concat("57077a4adfce0c4c3198e1d0dbacfd7f");
        }
    }

    @Test
    public void test3() {
        for (int i = 0; i < TIMES; i++) {
            StringBuilder builder1 = new StringBuilder();
            builder1.append("token1");
            builder1.append(':');
            builder1.append("w8iJhc3uDfinr%2BEv37VPuMqByQkuQm6z2zo88pLaiJxy7y2j%2BpAMHF6juh7jzXcM%2F6v7ND7tZwPweV9R3T1CBEN5j%2FBEnfkOsF4EQ7KeiAc%3D");

            StringBuilder builder2 = new StringBuilder();
            builder2.append("token2");
            builder2.append(':');
            builder2.append("57077a4adfce0c4c3198e1d0dbacfd7f");
        }
    }

    @Test
    public void test4() {
        long start = System.currentTimeMillis();
        redisService.keyname("token1", "YS1apbS587A6dMAir4x3Hpa9FJmE%2B2WC%2BvpsVJZNt%2Bjg52YuDQ%2BgTN0dzW7MznZzJwlfL9Da%2FLP3jsx%2B3YXi7PXdAjZAQ7ZUTI5cCNqNYII%3D");
        redisService.keyname("token2", "c8bf7e12402364cd81cfab38ebead7d6");
        long end = System.currentTimeMillis();
        System.out.println(end - start);
    }

    @Test
    public void test5() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < TIMES; i++) {
            String token1 = "tyOLwutI55RXWK4aHshWGcGGRK1J6jbxyrQIqFBeQergWpvurDxVBMXhm8SCy0XPXvfJbqe6BbavZi5%2FRDOA7xgR9qhE9JnWYkp%2FDcZlWxE%3D";
            String token2 = "c8bf7e12402364cd81cfab38ebead7d6";
            token2.equals(DigestUtils.md5Hex(token1));
            AESUtil.getUsername(token1);
        }
        long end = System.currentTimeMillis();
        System.out.println(end - start);
    }

    @Test
    public void test6() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 10; i++) {
            JSONObject object = new JSONObject();
            object.put("username", "p20160310240");
//            object.put("password", "asd123");
//            object.put("type", "student");
            String token1 = AESUtil.AESEncode(object.toJSONString());
            System.out.println(token1);
            String md5 = DigestUtils.md5Hex(token1);
            System.out.println(md5);
            AESUtil.getUsername(token1);
        }
        long end = System.currentTimeMillis();
        System.out.println(end - start);
    }

    @Test
    public void test7() {
        long start = System.currentTimeMillis();

        long end = System.currentTimeMillis();
        System.out.println(end - start);
    }

    @Test
    public void test8() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 1000; i++) {
            UUID.randomUUID().toString().replace('-', '\0');
        }
        long end = System.currentTimeMillis();
        System.out.println(end - start);
    }

    /*
    @Test
    public void test9() {
        SecretKey key = new SecretKeySpec(stringKey.getBytes(), "HS256");
//        SecretKey key = Keys.secretKeyFor(SignatureAlgorithm.HS256);

        Map<String, Object> map = new HashMap<>();
        map.put("username", "p20160310240");
        String sign = Jwts.builder().setClaims(map)
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 24))
                .signWith(key)
                .compact();

        System.out.println("token:" + sign);

        long start = System.currentTimeMillis();
        for (int i = 0; i < 1000; i++) {
            Claims claims = Jwts.parser().setSigningKey(key)
                    .parseClaimsJws(sign).getBody();
        }
        long end = System.currentTimeMillis();
        System.out.println(end - start);
    }
    */
}
