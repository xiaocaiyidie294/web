package cn.czh.web.controller;

import cn.czh.web.dao.NewsMapper;
import cn.czh.web.model.News;
import cn.czh.web.model.NewsExample;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SchoolNews {

    @Autowired
    NewsMapper newsMapper;

    @PostMapping(value = "/getNews/bootstrap")
    public Object getNews(@RequestParam("limit") String limit , @RequestParam("offset") String skip){
        NewsExample example = new NewsExample();
        example.setPageSize(Integer.parseInt(limit));
        example.setStartRow(Integer.parseInt(skip));
        example.setOrderByClause("`time` desc");

        List<News> newsList = newsMapper.selectByExampleWithBLOBs(example);

        JSONObject result = new JSONObject();
        result.put("rows",newsList);
        result.put("total",newsMapper.countByExample(new NewsExample()));

        return result;
    }

}
