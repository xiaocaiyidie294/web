package cn.czh.web.controller;

import cn.czh.web.util.ImageUtil;
import cn.czh.web.util.OCR;
import com.alibaba.fastjson.JSONObject;
import com.gargoylesoftware.htmlunit.*;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlRadioButtonInput;
import net.sourceforge.tess4j.TesseractException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.TimeUnit;

@RestController
public class SimLogin {

    Logger logger = LoggerFactory.getLogger(SimLogin.class);

    @Autowired
    RedisTemplate<Object,Object> redisTemplate;

    @PostMapping(value = "/sim/login")
    public Object simLogin(@Param("username") String username , @Param("password") String password , @Param("type") String type){

        logger.info("登录类型：" + type);
        logger.info("用户名：" + username);
        logger.info("密码：" + password);

        if(type==null || type.isEmpty()) return null;
        if(username==null || username.isEmpty()) return null;
        if(password==null || password.isEmpty()) return null;

        JSONObject result = new JSONObject();

        if(redisTemplate.opsForValue().getOperations().hasKey("SimLogin:" + username)){
            result.put("cookie",redisTemplate.opsForValue().get("SimLogin:" + username));
            logger.info("redis缓存");
            return result;
        }

        WebClient client = new WebClient(BrowserVersion.CHROME);
        client.getOptions().setThrowExceptionOnScriptError(false);
        client.getOptions().setThrowExceptionOnFailingStatusCode(false);
        client.getOptions().setActiveXNative(false);
        client.getOptions().setCssEnabled(false);
        client.getOptions().setJavaScriptEnabled(true);
        client.setAjaxController(new NicelyResynchronizingAjaxController());
        client.setAlertHandler((page, s) -> {
            logger.info(s);
            result.put("tip", s);
        });

        try {
            //清空cookie
            client.getCookieManager().clearCookies();

            //获取页面
            HtmlPage page = client.getPage("http://jwxt.jmpt.cn:8125/JspHelloWorld/login.jsp");
            logger.info(String.valueOf(client.getCookieManager().getCookies()));
            String cookie = client.getCookieManager().getCookie("JSESSIONID").getValue();

            if("teacher".equals(type)){
                HtmlRadioButtonInput radioButtonInput = (HtmlRadioButtonInput) page.getElementById("teacher");
                radioButtonInput.setChecked(true);
            }

            //填充表单
            HtmlForm form = page.getForms().get(0);
            form.getInputByName("username").setValueAttribute(username);
            form.getInputByName("password").setValueAttribute(password);

            for(int i=1 ; i<=5 ; i++){
                URL url = new URL("http://jwxt.jmpt.cn:8125/JspHelloWorld/authImg");
                URLConnection connection = url.openConnection();
                connection.setRequestProperty("Cookie", "JSESSIONID="+cookie);
                BufferedImage buffered = ImageIO.read(connection.getInputStream());
                buffered = buffered.getSubimage(1, 1, buffered.getWidth() - 2, buffered.getHeight() - 2);
                ImageUtil.binaryzation(buffered);

                String captcha = null;
                try {
                    captcha = OCR.getTesseract().doOCR(buffered).trim();
                } catch (TesseractException e) {
                    e.printStackTrace();
                }

                logger.info("正在进行第" + i + "次OCR验证码识别，验证码：" + captcha);
                if(captcha != null && captcha.matches("^[A-Za-z0-9]{3}$")){
                    form.getInputByName("validate").setValueAttribute(captcha);
                    HtmlPage menu = form.getInputByName("login").click();

                    logger.info(menu.getTitleText());
                    if("请选择对应的功能".equals(menu.getTitleText())){
                        logger.info("模拟登录成功");
                        result.remove("tip");
                        result.remove("error");
                        result.put("cookie" , "JSESSIONID="+cookie);
                        redisTemplate.opsForValue().set("SimLogin:" + username , result.get("cookie") , 5, TimeUnit.MINUTES);
                        break;
                    }else{
                        if(result.containsKey("tip") && !result.getString("tip").contains("验证码")){
                            break;
                        }

                        if(result.containsKey("error")){
                            break;
                        }
                    }
                }
            }

        } catch (IOException e) {
            result.put("error","IO Exception");
            e.printStackTrace();
        } finally {
            client.close();
        }

        logger.info("result:" + result.toJSONString() + "\n");
        return result.toJSONString();
    }
}
