package cn.czh.web.controller;

import cn.czh.web.dao.*;
import cn.czh.web.model.*;
import cn.czh.web.util.AESUtil;
import cn.czh.web.web.COS;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
public class ManagerSystem {

    @Autowired
    CircleMapper circleMapper;

    @GetMapping("/manager/circle")
    public Object getCircle(@RequestParam("offset") int offset , @RequestParam("limit") int limit , HttpServletRequest request){
        JSONObject result = new JSONObject();

        String userAgent = request.getHeader("User-Agent");
        boolean isIOS = userAgent.contains("iPhone") || userAgent.contains("iPad");

        String username = (String) request.getAttribute("username");

        CircleExample example = new CircleExample();
        example.setPageSize(limit);
        example.setStartRow(offset);
        example.setUsername(username);
        example.setOrderByClause("`circle_id` desc");
        List<CircleModel> circles = circleMapper.selectModelByExampleWithBLOBs(example);


        for (CircleModel model : circles) {
            if(model.getImages() != null){
                String[] urls = model.getImages().split(" ");
                for (int i = 0; i < urls.length; i++) {
                    if(isIOS){
                        urls[i] = COS.getSmallJpg(COS.getImageUrl(urls[i]));
                    }else{
                        urls[i] = COS.getSmallWebp(COS.getImageUrl(urls[i]));
                    }
                }
                model.setUrls(urls);
                model.setImages(null);
            }

            if(model.getHead() != null){
                if(isIOS){
                    model.setHead(COS.getHeadJpg(COS.getImageUrl(model.getHead())));
                }else{
                    model.setHead(COS.getHeadWebp(COS.getImageUrl(model.getHead())));
                }
            }else{
                if(isIOS){
                    model.setHead(COS.getHeadJpg(COS.getImageUrl(COS.DEFAULT_HEAD_UUID)));
                }else{
                    model.setHead(COS.getHeadWebp(COS.getImageUrl(COS.DEFAULT_HEAD_UUID)));
                }
            }
        }

        result.put("rows",circles);
        result.put("total",circleMapper.countByExample(new CircleExample()));
        return result;
    }

    @Autowired
    AdminMapper adminMapper;

    @PostMapping("/manager/circle/delete")
    public void delectCircle(@RequestParam("id") Integer id , @RequestParam("token") String token){
        String username = AESUtil.getUsername(token);
        String password = AESUtil.getPassword(token);
        AdminExample example = new AdminExample();
        example.createCriteria().andUsernameEqualTo(username).andPasswordEqualTo(password);
        if(adminMapper.countByExample(example) > 0){
            circleMapper.deleteByPrimaryKey(id);
        }
    }

    @Autowired
    LoveWallMapper loveWallMapper;

    @GetMapping(value = "/manager/wall")
    public Object getLoveWall(@RequestParam("offset") int offset , @RequestParam("limit") int limit){
        LoveWallExample example = new LoveWallExample();
        example.setPageSize(limit);
        example.setStartRow(offset);
        example.setOrderByClause("`time` desc");
        List<LoveWall> walls = loveWallMapper.selectByExampleWithBLOBs(example);

        JSONObject result = new JSONObject();
        result.put("rows",walls);
        result.put("total",loveWallMapper.countByExample(new LoveWallExample()));

        return result;
    }

    @PostMapping(value = "/manager/wall/delete")
    public void deleteLoveWall(@RequestParam("id") Integer id, @RequestParam("token") String token){
        String username = AESUtil.getUsername(token);
        String password = AESUtil.getPassword(token);
        AdminExample example = new AdminExample();
        example.createCriteria().andUsernameEqualTo(username).andPasswordEqualTo(password);
        if(adminMapper.countByExample(example) > 0){
            loveWallMapper.deleteByPrimaryKey(id);
        }
    }

    @Autowired
    OpinionMapper opinionMapper;

    @GetMapping(value = "/manager/opinion")
    public Object getOpinion(@RequestParam("offset") int offset , @RequestParam("limit") int limit , @RequestParam("token") String token){
        JSONObject result = new JSONObject();

        OpinionExample example = new OpinionExample();
        example.setPageSize(limit);
        example.setStartRow(offset);
        example.setOrderByClause("`id` desc");
        List<OpinionWithBLOBs> list = opinionMapper.selectByExampleWithBLOBs(example);

        result.put("rows",list);
        result.put("total",opinionMapper.countByExample(new OpinionExample()));
        return result;
    }

    @PostMapping(value = "/manager/opinion/delete")
    public void deleteOpinion(@RequestParam("id") Integer id, @RequestParam("token") String token){
        String username = AESUtil.getUsername(token);
        String password = AESUtil.getPassword(token);
        AdminExample example = new AdminExample();
        example.createCriteria().andUsernameEqualTo(username).andPasswordEqualTo(password);
        if(adminMapper.countByExample(example) > 0){
            opinionMapper.deleteByPrimaryKey(id);
        }
    }

    @Autowired
    VisitLogMapper logMapper;

    @GetMapping(value = "/manager/visit")
    public Object getRequests(@RequestParam("token") String token){
        String username = AESUtil.getUsername(token);
        String password = AESUtil.getPassword(token);
        AdminExample adminExample = new AdminExample();
        adminExample.createCriteria().andUsernameEqualTo(username).andPasswordEqualTo(password);
        if(adminMapper.countByExample(adminExample) == 0){
            return null;
        }

        JSONObject result = new JSONObject();

        List<String> labels = new LinkedList<>();
        List<Long> counts = new LinkedList<>();

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY , 0);
        calendar.set(Calendar.MINUTE , 0);
        calendar.set(Calendar.SECOND , 0);
        for (int i = 0; i < 7; i++) {
            VisitLogExample example = new VisitLogExample();
            example.createCriteria().andTimeBetween(calendar.getTime() , new Date(calendar.getTimeInMillis() + 1000*60*60*24));

            SimpleDateFormat format = new SimpleDateFormat("MM-dd");
            labels.add(format.format(calendar.getTime()));
            counts.add(logMapper.countByExample(example));

            calendar.add(Calendar.DAY_OF_YEAR , -1);
        }

        result.put("labels",labels);
        result.put("counts",counts);
        return result;
    }
}
