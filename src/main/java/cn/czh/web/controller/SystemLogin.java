package cn.czh.web.controller;

import cn.czh.web.dao.AdminMapper;
import cn.czh.web.model.AdminExample;
import cn.czh.web.model.User;
import cn.czh.web.util.AESUtil;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SystemLogin {

    @Autowired
    AdminMapper adminMapper;

    @PostMapping(value = "/manager/login")
    public Object login(@RequestParam("username") String username , @RequestParam("password") String password){

        AdminExample example = new AdminExample();
        example.createCriteria().andUsernameEqualTo(username).andPasswordEqualTo(password);
        if(adminMapper.countByExample(example) > 0){
            JSONObject object = new JSONObject();
            object.put("username",username);
            object.put("password",password);
            String token = AESUtil.AESEncode(object.toJSONString());

            JSONObject result = new JSONObject();
            result.put("token",token);
            return result;
        }
        return null;
    }

}
