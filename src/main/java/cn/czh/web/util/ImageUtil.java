package cn.czh.web.util;

import java.awt.image.BufferedImage;

public class ImageUtil {
    /**
     * 图像二值化
     * @param image
     */
    public static void binaryzation(BufferedImage image){
        int NUM = 150;
        for(int x=0 ; x<image.getWidth() ; x++){
            for(int y=0 ; y<image.getHeight() ; y++){
                int pixel = image.getRGB(x, y);
                float red= (pixel & 0xff0000) >> 16;
                float green = (pixel & 0xff00) >> 8;
                float blue = (pixel & 0xff);
                float avg = (red + green + blue)/3;
                image.setRGB(x, y, avg<=NUM ? 0 : Integer.MAX_VALUE);
            }
        }
    }
}
