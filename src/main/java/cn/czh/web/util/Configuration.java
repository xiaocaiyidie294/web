package cn.czh.web.util;

public class Configuration {
    public static final String commitUrl = "http://jwxt.jmpt.cn:8125/JspHelloWorld/servlets/CommonServlet";
    public static final String studentMainUrl = "http://jwxt.jmpt.cn:8125/JspHelloWorld/menu.jsp";
    public static final String teacherMainUrl = "http://jwxt.jmpt.cn:8125/JspHelloWorld/menuJs.jsp";
    public static final int cookieTime = 1000*60*3;
    public static final int imageMaxLength = 1024*1024*5;
}
