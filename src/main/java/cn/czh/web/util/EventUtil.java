package cn.czh.web.util;

import cn.czh.web.WebApplication;
import cn.czh.web.dao.CookieMapper;
import cn.czh.web.dao.CourseMapper;
import cn.czh.web.dao.EventMapper;
import cn.czh.web.model.Event;
import cn.czh.web.web.Application;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.Date;

public class EventUtil {

    @Autowired
    private EventMapper eventMapper;

    public static final String LOGIN = "登录";
    public static final String GET_SCORE = "爬取成绩";
    public static final String BLACK_FILTER = "过滤黑名单";

    public static void insertEvent(String name, String detail, String username) {
        EventMapper eventMapper = (EventMapper) WebApplication.getInstance().getBean("eventMapper");
        Event event = new Event();
        event.setName(name);
        event.setDetail(detail);
        event.setUser(username);
        event.setTime(new Date());
        eventMapper.insert(event);
    }

}
