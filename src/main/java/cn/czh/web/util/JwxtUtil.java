package cn.czh.web.util;

import cn.czh.web.WebApplication;
import cn.czh.web.model.Score;
import cn.czh.web.model.User;
import cn.czh.web.service.RedisService;
import cn.czh.web.web.Application;
import com.alibaba.fastjson.JSONObject;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class JwxtUtil {
    public static String getCookie(String username , String password){
        RedisService redisService = (RedisService) WebApplication.getInstance().getBean("redisService");
        if(redisService.containsKey(redisService.keyname("cookie",username))){
            return (String) redisService.get(redisService.keyname("cookie",username));
        }else{
            JSONObject loginResult = LoginUtil.login("student",username,password);
            if (!loginResult.containsKey("cookie")) {
                if(loginResult.containsKey("tip")){
                    return null;
                }
            }else{
                redisService.set(redisService.keyname("cookie",username) , loginResult.getString("cookie") , 5 , TimeUnit.MINUTES);
                return loginResult.getString("cookie");
            }
        }

        return null;
    }

    public static User getUserInfo(String type , String username , String password , String cookie) throws IOException {
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);

        if("student".equals(type)){
            Document document = Jsoup.parse(WebUtil.get(Configuration.studentMainUrl , cookie).body().string());
            String text = document.text();
            System.out.println(text);
            user.setName(TextUtil.findText(text , "姓名：","，"));
            user.setSex(TextUtil.findText(text,"性别：","，"));
            user.setBanji(TextUtil.findText(text,"欢迎您，","，"));
            user.setBirthday(TextUtil.findText(text,"出生日期：","，"));
            user.setId(TextUtil.findText(text,"身份证号："," "));
            user.setTel(TextUtil.findText(text,"电话：","；"));
            user.setAddress(TextUtil.findText(text,"地址：","；"));
        }else if("teacher".equals(type)){
            Document document = Jsoup.parse(WebUtil.get(Configuration.teacherMainUrl , cookie).body().string());
            String text = document.text();
            System.out.println(text);
            String name = TextUtil.findText(text,"您，"," ");
            user.setName(name.substring(name.indexOf('，')+1 , name.length()));
            user.setBanji(TextUtil.findText(text,"所在部门: ","。"));
        }

        RedisService redisService = (RedisService) WebApplication.getInstance().getBean("redisService");
        redisService.set(redisService.keyname("cookie",username) , cookie , 5 , TimeUnit.MINUTES);
        user.setUpdateTime(new Date());
        return user;
    }

    public static List<Score> getScores(String username , String cookie) throws IOException {
        String params = "MaxID=0&"
                + "PDF=&"
                + "pageId=000201&"
                + "actionId=009";
        Request request = new Request.Builder().url(Configuration.commitUrl).addHeader("Cookie",cookie)
                .post(RequestBody.create(MediaType.parse("application/x-www-form-urlencoded") ,params)).build();
        Response response = Application.getHttpClient().newCall(request).execute();
        String text = response.body().string();
        Document document = Jsoup.parse(text);
        String BjCode = document.select("input[name=BjCode]").get(0).attr("value");
        String pageId = document.select("input[name=pageId]").get(0).attr("value");

        params = "CxXq=%C8%AB%B2%BF%D1%A7%C6%DA&JdKc=1&"
                + "Xh=" + username.toUpperCase() + "&"
                + "PyzyCode=" + BjCode + "&"
                + "BjCode=" + BjCode + "&"
                + "pageId=" + pageId + "&"
                + "actionId=register";
        request = new Request.Builder().url(Configuration.commitUrl).addHeader("Cookie",cookie)
                .post(RequestBody.create(MediaType.parse("application/x-www-form-urlencoded") ,params)).build();
        response = Application.getHttpClient().newCall(request).execute();
        text = response.body().string();
//        System.out.println(text);
        Elements trs = Jsoup.parse(text).select("body > form > table > tbody > tr > td > table > tbody > tr:nth-child(2) > td:nth-child(3) > table > tbody > tr:nth-child(3) > td > table > tbody").select("tr");
        List<Score> scores = new ArrayList<Score>();
        for(int i=1 ; i<trs.size()-2 ; i++){
            Score score = new Score();
            Elements tds = trs.get(i).select("td");
            int len = tds.size();
            score.setSid(username);
            score.setRemark2(tds.get(len-1).text());
            score.setRemark(tds.get(len-2).text());
            score.setZf(tds.get(len-3).text());
            score.setKsf(tds.get(len-4).text());
            score.setPsf(tds.get(len-5).text());
            score.setXq(tds.get(len-6).text());
            score.setCourse(tds.get(len-10).text());
            score.setCode(tds.get(len-11).text());
            scores.add(score);
        }

        RedisService redisService = (RedisService) WebApplication.getInstance().getBean("redisService");
        redisService.set(redisService.keyname("cookie",username) , cookie , 5 , TimeUnit.MINUTES);
        return scores;
    }
}
