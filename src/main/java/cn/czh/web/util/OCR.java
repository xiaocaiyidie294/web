package cn.czh.web.util;

import net.sourceforge.tess4j.Tesseract;

public class OCR {

    private static Tesseract tesseract;

    public static Tesseract getTesseract(){
        if(tesseract == null){
            tesseract = new Tesseract();
            tesseract.setDatapath("C:\\CAI\\ocr\\tessdata");
//            tesseract.setDatapath("D:\\img\\tessdata");
            tesseract.setLanguage("cai");
        }
        return tesseract;
    }

}
