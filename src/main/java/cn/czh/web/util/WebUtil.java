package cn.czh.web.util;

import cn.czh.web.web.Application;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import java.io.IOException;

public class WebUtil {
    public static Response get(String url , String cookie) throws IOException {
        return Application.getHttpClient().newCall(new Request.Builder()
                .get()
                .url(url)
                .addHeader("Cookie", cookie)
                .build()).execute();
    }

    public static Response post(String url , String cookie , String params) throws IOException {
        return Application.getHttpClient().newCall(new Request.Builder()
                .post(RequestBody.create(MediaType.parse("application/x-www-form-urlencoded") ,params))
                .url(url)
                .addHeader("Cookie", cookie)
                .build()).execute();
    }

}
