package cn.czh.web.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextUtil {
    /**
     * 检查参数有效性
     * @param text
     * @return 参数其中有一个为空或者长度为0，则返回false
     */
    public static boolean checkTextValid(String ... text){
        boolean ok = true;
        for(String s : text){
            if(s==null || s.length()==0){
                ok=false;
                break;
            }
        }
        return ok;
    }

    /**
     * 根据前后关键字找文本
     * 注意不能含有 ( ) 字符
     * @param text
     * @param start
     * @param end
     * @return
     */
    public static String findText(String text , String start , String end){
        Pattern p = Pattern.compile(start + "(.+?)" + end);
        Matcher m = p.matcher(text);
        String result = "";
        while(m.find()) {
            result = m.group(1);
        }

        return result;
    }

    public static String findText2(String text , String start , String end){
        Pattern p = Pattern.compile(start + "(.+?)" + end);
        Matcher m = p.matcher(text);
        String result = "Not Find";
        while(m.find()) {
            result = m.group(1);
        }

        return result;
    }
}
