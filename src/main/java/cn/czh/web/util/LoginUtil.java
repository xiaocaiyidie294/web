package cn.czh.web.util;

import cn.czh.web.service.RedisService;
import cn.czh.web.web.Application;
import com.alibaba.fastjson.JSONObject;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

@Component
public class LoginUtil {

    public static JSONObject login(String type , String username , String password){
        JSONObject object = new JSONObject();

        OkHttpClient httpClient = Application.getHttpClient();
        try {
            Response httpResponse = httpClient.newCall(new Request.Builder().post(new FormBody.Builder()
                    .add("type",type)
                    .add("username",username)
                    .add("password",password)
                    .build())
                    .url("https://www.caizhenhang.cn/sim/login").build()).execute();

            if(httpResponse.code() == 200){
                JSONObject jsonObject = JSONObject.parseObject(httpResponse.body().string());
                if (jsonObject.containsKey("cookie")) {
                    //登陆成功
                    object.put("cookie",jsonObject.getString("cookie"));
                } else {
                    //模拟登陆失败
                    object.put("tip",jsonObject.getString("tip"));
                }

            }else{
                //连接模拟登陆服务器失败
                object.put("error","连接模拟登陆服务器失败");
            }
        } catch (IOException e) {
            //异常错误
            e.printStackTrace();
            object.put("error",e.getMessage());
        }

        return object;
    }




}
