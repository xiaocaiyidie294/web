package cn.czh.web.service;

import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.lang.reflect.Method;
import java.util.Arrays;

@Configuration
public class KeyGeneratorConfig {

    @Bean("myKeyGenerator")
    public KeyGenerator keyGenerator(){
//        return (target, method, params) -> method.getName() + Arrays.asList(params).toString();

        return (target, method, params) -> {
            String s = "";
            for (int i = 0; i < params.length; i++) {
                s += params[i].toString();
                if(i<params.length-1) s += ":";
            }

            return method.getName() + ":" + s;
        };

    }
}
