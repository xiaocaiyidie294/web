package cn.czh.web.service;

import cn.czh.web.dao.NewsMapper;
import cn.czh.web.model.News;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.processor.PageProcessor;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class NewsChild implements PageProcessor {

	Site site = Site.me().setCharset("utf-8").setSleepTime(100).setRetryTimes(3).setRetrySleepTime(100);
	Logger logger = LoggerFactory.getLogger(NewsChild.class);

	@Autowired
	NewsMapper newsMapper;
	
	@Override
	public Site getSite() {
		return site;
	}

	@Override
	public void process(Page page) {
		logger.info("[新闻爬虫]页面爬取成功：" + page.getUrl().toString());

		Document document = Jsoup.parse(page.getHtml().toString());
		String title = "";
		String time = "";
		String type = "";
		
		if(page.getUrl().toString().contains("www.jmpt.cn")){
			type = "学院";
			String content = document.select("body > div > div.wraper > div.newContent.clearfix").toString();
			title = Jsoup.parse(content).getElementsByClass("title thA").select("h2").text();
			
			String regex = "\\d{4}-\\d{1,2}-\\d{1,2} \\d{2}:\\d{2}:\\d{2}";
			Pattern pattern = Pattern.compile(regex);
			Matcher m = pattern.matcher(content);
			m.find();
			time = m.group();
		}else if(page.getUrl().toString().contains("clx.jmpt.cn")){
			type = "材料系";
			String content = document.select("#content > div.list_r > div").toString();
			title = Jsoup.parse(content).getElementsByClass("arc_con").select("h3").text();
			
			String regex = "\\d{4}-\\d{1,2}-\\d{1,2} \\d{2}:\\d{2}:\\d{2}";
			Pattern pattern = Pattern.compile(regex); 
			Matcher m = pattern.matcher(content);
			m.find();
			time = m.group();
		}else if(page.getUrl().toString().contains("dzx.jmpt.cn")){
			type = "电子系";
			String content = document.select("body > table:nth-child(3) > tbody > tr > td:nth-child(2) > div > div.newContent.clearfix").toString();
			title = Jsoup.parse(content).getElementsByClass("title thA").select("h2").text();
			
			String regex = "\\d{4}-\\d{1,2}-\\d{1,2} \\d{2}:\\d{2}:\\d{2}";
			Pattern pattern = Pattern.compile(regex); 
			Matcher m = pattern.matcher(content);
			m.find();
			time = m.group();
		}else if(page.getUrl().toString().contains("jgx.jmpt.cn")){
			type = "经管系";
			String content = document.select("body > div.w1024.mb > div.right_list.fr > div.new_content").toString();
			title = Jsoup.parse(content).getElementsByClass("new_content").select("h3").text();
			
			String regex = "\\d{4}-\\d{1,2}-\\d{1,2} \\d{2}:\\d{2}:\\d{2}";
			Pattern pattern = Pattern.compile(regex); 
			Matcher m = pattern.matcher(content);
			m.find();
			time = m.group();
		}else if(page.getUrl().toString().contains("jdx.jmpt.cn")){
			type = "机电系";
			String content = document.select("#frame-main-body").toString();
			title = Jsoup.parse(content).getElementsByClass("title thA").select("h2").text();
			
			String regex = "\\d{4}-\\d{1,2}-\\d{1,2} \\d{2}:\\d{2}:\\d{2}";
			Pattern pattern = Pattern.compile(regex); 
			Matcher m = pattern.matcher(content);
			m.find();
			time = m.group();
		}else if(page.getUrl().toString().contains("jyx.jmpt.cn")){
			type = "教育系";
			String content = document.select("body > table > tbody > tr:nth-child(2) > td > div").toString();
			title = Jsoup.parse(content).getElementsByClass("title").select("h2").text();
			
			String regex = "\\d{4}-\\d{1,2}-\\d{1,2} \\d{2}:\\d{2}:\\d{2}";
			Pattern pattern = Pattern.compile(regex); 
			Matcher m = pattern.matcher(content);
			m.find();
			time = m.group();
		}else if(page.getUrl().toString().contains("wyx.jmpt.cn")){
			type = "外语系";
			String content = document.select("body > table > tbody > tr:nth-child(2) > td > div").toString();
			title = Jsoup.parse(content).getElementsByClass("title").select("h2").text();
			
			String regex = "\\d{4}-\\d{1,2}-\\d{1,2} \\d{2}:\\d{2}:\\d{2}";
			Pattern pattern = Pattern.compile(regex); 
			Matcher m = pattern.matcher(content);
			m.find();
			time = m.group();
		}else if(page.getUrl().toString().contains("ysx.jmpt.cn")){
			type = "艺术系";
			String content = document.select("body > table > tbody > tr:nth-child(4) > td > div.a2 > div > div.title.thA").toString();
			title = Jsoup.parse(content).getElementsByClass("title thA").select("h2").text();
			
			String regex = "\\d{4}-\\d{1,2}-\\d{1,2} \\d{2}:\\d{2}:\\d{2}";
			Pattern pattern = Pattern.compile(regex); 
			Matcher m = pattern.matcher(content);
			m.find();
			time = m.group();
		}
			
		logger.info("[新闻爬虫]标题" + title);
		logger.info("[新闻爬虫]时间" + time);
		logger.info("[新闻爬虫]来源" + type);

		News news = new News();
		news.setTitle(title);
		news.setTime(time);
		news.setUrl(page.getUrl().toString());
		news.setType(type);
		newsMapper.insert(news);
	}

}
