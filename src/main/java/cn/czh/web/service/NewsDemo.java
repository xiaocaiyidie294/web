package cn.czh.web.service;

import cn.czh.web.dao.NewsMapper;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;

import java.util.ArrayList;
import java.util.List;

@Component
public class NewsDemo implements PageProcessor{
	Site site = Site.me().setCharset("utf-8").setSleepTime(100).setRetryTimes(3).setRetryTimes(100);
	Logger logger = Logger.getLogger(NewsDemo.class);

	@Autowired
	NewsMapper newsMapper;
	@Autowired
	NewsChild newsChild;
	
	@Override
	public Site getSite() {
		return site;
	}

	@Override
	public void process(Page page) {
		logger.info("[新闻爬虫] 正在爬取站点:" + page.getUrl().toString());
		List<String> links = new ArrayList<String>();
		
		if(page.getUrl().toString().contains("www.jmpt.cn")){
			links = page.getHtml().css("body > div > div.wraper > div > div.fr.mainDetail > div.new2 > div.info_cont.tbA").links().all();
		}else if(page.getUrl().toString().contains("clx.jmpt.cn")){
			links = page.getHtml().css("#ulstyle_list").links().all();
		}else if(page.getUrl().toString().contains("dzx.jmpt.cn")){
			links = page.getHtml().css("body > table:nth-child(3) > tbody > tr > td:nth-child(2) > div > div > div.fr.mainDetail > div.new2 > div.info_cont.tbA").links().all();
		}else if(page.getUrl().toString().contains("jgx.jmpt.cn")){
			links = page.getHtml().css("body > div.w1024.mb > div.right_list.fr > ul").links().all();
		}else if(page.getUrl().toString().contains("jdx.jmpt.cn")){
			links = page.getHtml().css("#frame-main > div.newsList > div").links().all();
		}else if(page.getUrl().toString().contains("jyx.jmpt.cn")){
			links = page.getHtml().css("body > table > tbody > tr:nth-child(2) > td > div > div.d2 > div > div.tbA.clearfix").links().all();
		}else if(page.getUrl().toString().contains("wyx.jmpt.cn")){
			links = page.getHtml().css("body > table > tbody > tr:nth-child(2) > td > div > div.d2 > div > div.tbA.clearfix").links().all();
		}else if(page.getUrl().toString().contains("ysx.jmpt.cn")){
			links = page.getHtml().css("body > table > tbody > tr:nth-child(3) > td > div.clearfix.mt10 > div.fr.mainDetail").links().all();
		}
		
		logger.info(links);
		boolean skip = false;
		for(int i=0 ; i<links.size() ; i++){
			if(newsMapper.countByUrl(links.get(i)) >= 1){
                System.out.println("skip:" + links.get(i));
                logger.info("[新闻爬虫]跳过站点:" + links.get(i));
				skip = true;
				break;
			}
			
			Spider.create(newsChild).addUrl(links.get(i)).thread(5).run();
		}

		if(!skip){
			Elements elements = Jsoup.parse(page.getHtml().toString()).select("a");
			for(Element link : elements){
				if("下一页".equals(link.text())){
					page.addTargetRequest(link.attr("href"));
				}
			}
		}
	}
	
}
