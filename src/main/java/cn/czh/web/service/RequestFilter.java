package cn.czh.web.service;

import cn.czh.web.dao.VisitLogMapper;
import cn.czh.web.model.VisitLog;
import cn.czh.web.model.VisitLogWithBLOBs;
import cn.czh.web.util.TextUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.undertow.servlet.handlers.ServletRequestContext;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.RequestContext;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.zip.GZIPOutputStream;

@Component
@WebFilter(urlPatterns = "/**")
@Order(1)
public class RequestFilter implements Filter {

    @Autowired
    RedisService redisService;
    @Autowired
    VisitLogMapper logMapper;

    private Logger logger = LoggerFactory.getLogger(RequestFilter.class);

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        VisitLogWithBLOBs log = new VisitLogWithBLOBs();
        long start = System.currentTimeMillis();

        //请求的路径
        String path = request.getRequestURI();
        //获取请求参数信息
        String params = JSON.toJSONString(request.getParameterMap());
        //请求ip
        String ip = request.getRemoteAddr();
        //请求方法
        String method = request.getMethod();
        String userAgent = request.getHeader("User-Agent");

        logger.info(params);

        response.setHeader("Access-Control-Allow-Origin", "*");

//        ResponseWrapper wrapper = new ResponseWrapper((HttpServletResponse) servletResponse);
        filterChain.doFilter(servletRequest, servletResponse);

        String result = (String) request.getAttribute("result");
        String username = (String) request.getAttribute("username");

        if(result!=null){
            servletResponse.getWriter().write(result);
        }else{
            result = "请求被controller处理";
        }

        long end = System.currentTimeMillis();
        int statusCode = response.getStatus();

        log.setUrl(path);
        log.setParams(params);
        log.setIp(ip);
        log.setType(method);
        log.setUsername(username);
        log.setSpend((short) (end - start));
        log.setCode((short) statusCode);
        log.setResult(result);
        log.setAgent(userAgent);

        logMapper.insert(log);
    }

}
