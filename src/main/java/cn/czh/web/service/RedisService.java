package cn.czh.web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class RedisService {

    @Autowired
    private RedisTemplate<Object ,Object> redisTemplate;

    public void set(String key, Object value) {
        redisTemplate.opsForValue().set(key , value);
    }

    public void set(String key , Object value , long timeout , TimeUnit unit){
        redisTemplate.opsForValue().set(key, value, timeout, unit);
    }

    public Object get(String key){
        return redisTemplate.opsForValue().get(key);
    }

    public void addSet(String key , Object ... value){
        redisTemplate.opsForSet().add(key , value);
    }

    public boolean containsSetValue(String key , Object value){
        return redisTemplate.opsForSet().isMember(key,value);
    }

    public boolean containsKey(String key){
        return redisTemplate.hasKey(key);
    }

    public String keyname(String ... names){
        String s = "";
        for (int i = 0; i < names.length; i++) {
            s += names[i];
            if(i != names.length-1){
                s += ":";
            }
        }
        return s;
    }

}
