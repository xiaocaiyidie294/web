package cn.czh.web.dao;

import cn.czh.web.model.Circle;
import cn.czh.web.model.CircleExample;
import cn.czh.web.model.CircleModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CircleMapper {
    long countByExample(CircleExample example);

    int deleteByExample(CircleExample example);

    int deleteByPrimaryKey(Integer circleId);

    int insert(Circle record);

    int insertSelective(Circle record);

    List<Circle> selectByExampleWithBLOBs(CircleExample example);

    List<Circle> selectByExample(CircleExample example);

    List<CircleModel> selectModelByExampleWithBLOBs(CircleExample example);

    Circle selectByPrimaryKey(Integer circleId);

    int updateByExampleSelective(@Param("record") Circle record, @Param("example") CircleExample example);

    int updateByExampleWithBLOBs(@Param("record") Circle record, @Param("example") CircleExample example);

    int updateByExample(@Param("record") Circle record, @Param("example") CircleExample example);

    int updateByPrimaryKeySelective(Circle record);

    int updateByPrimaryKeyWithBLOBs(Circle record);

    int updateByPrimaryKey(Circle record);
}