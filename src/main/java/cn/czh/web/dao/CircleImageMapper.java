package cn.czh.web.dao;

import cn.czh.web.model.CircleImage;
import cn.czh.web.model.CircleImageExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CircleImageMapper {
    long countByExample(CircleImageExample example);

    int deleteByExample(CircleImageExample example);

    int insert(CircleImage record);

    int insertSelective(CircleImage record);

    List<CircleImage> selectByExample(CircleImageExample example);

    int updateByExampleSelective(@Param("record") CircleImage record, @Param("example") CircleImageExample example);

    int updateByExample(@Param("record") CircleImage record, @Param("example") CircleImageExample example);
}