package cn.czh.web.dao;

import cn.czh.web.model.CircleCommentLike;
import cn.czh.web.model.CircleCommentLikeExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CircleCommentLikeMapper {
    long countByExample(CircleCommentLikeExample example);

    int deleteByExample(CircleCommentLikeExample example);

    int insert(CircleCommentLike record);

    int insertSelective(CircleCommentLike record);

    List<CircleCommentLike> selectByExample(CircleCommentLikeExample example);

    int updateByExampleSelective(@Param("record") CircleCommentLike record, @Param("example") CircleCommentLikeExample example);

    int updateByExample(@Param("record") CircleCommentLike record, @Param("example") CircleCommentLikeExample example);

    Map<String , Object> updateCircleCommentLike(@Param("id") Integer id, @Param("username") String username);
}