package cn.czh.web.dao;

import cn.czh.web.model.BanJi;
import cn.czh.web.model.BanJiExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BanJiMapper {
    long countByExample(BanJiExample example);

    int deleteByExample(BanJiExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(BanJi record);

    int insertSelective(BanJi record);

    List<BanJi> selectByExample(BanJiExample example);

    BanJi selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") BanJi record, @Param("example") BanJiExample example);

    int updateByExample(@Param("record") BanJi record, @Param("example") BanJiExample example);

    int updateByPrimaryKeySelective(BanJi record);

    int updateByPrimaryKey(BanJi record);
}