package cn.czh.web.dao;

import cn.czh.web.model.Head;
import cn.czh.web.model.HeadExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface HeadMapper {
    long countByExample(HeadExample example);

    int deleteByExample(HeadExample example);

    int deleteByPrimaryKey(String username);

    int insert(Head record);

    int insertSelective(Head record);

    List<Head> selectByExample(HeadExample example);

    Head selectByPrimaryKey(String username);

    int updateByExampleSelective(@Param("record") Head record, @Param("example") HeadExample example);

    int updateByExample(@Param("record") Head record, @Param("example") HeadExample example);

    int updateByPrimaryKeySelective(Head record);

    int updateByPrimaryKey(Head record);
}