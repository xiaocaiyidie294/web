package cn.czh.web.dao;

import cn.czh.web.model.CircleComment;
import cn.czh.web.model.CircleCommentExample;
import cn.czh.web.model.CircleCommentModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CircleCommentMapper {
    long countByExample(CircleCommentExample example);

    int deleteByExample(CircleCommentExample example);

    int deleteByPrimaryKey(Integer commentId);

    int insert(CircleComment record);

    int insertSelective(CircleComment record);

    List<CircleComment> selectByExampleWithBLOBs(CircleCommentExample example);

    List<CircleComment> selectByExample(CircleCommentExample example);

    List<CircleCommentModel> selectModelByExampleWithBLOBs(CircleCommentExample example);

    CircleComment selectByPrimaryKey(Integer commentId);

    int updateByExampleSelective(@Param("record") CircleComment record, @Param("example") CircleCommentExample example);

    int updateByExampleWithBLOBs(@Param("record") CircleComment record, @Param("example") CircleCommentExample example);

    int updateByExample(@Param("record") CircleComment record, @Param("example") CircleCommentExample example);

    int updateByPrimaryKeySelective(CircleComment record);

    int updateByPrimaryKeyWithBLOBs(CircleComment record);

    int updateByPrimaryKey(CircleComment record);

    void insertCircleComment(@Param("circleId") Integer circleId, @Param("username") String username, @Param("answerId") Integer answerId, @Param("text") String text);
}