package cn.czh.web.dao;

import cn.czh.web.model.VisitLog;
import cn.czh.web.model.VisitLogExample;
import cn.czh.web.model.VisitLogWithBLOBs;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface VisitLogMapper {
    long countByExample(VisitLogExample example);

    int deleteByExample(VisitLogExample example);

    int insert(VisitLogWithBLOBs record);

    int insertSelective(VisitLogWithBLOBs record);

    List<VisitLogWithBLOBs> selectByExampleWithBLOBs(VisitLogExample example);

    List<VisitLog> selectByExample(VisitLogExample example);

    int updateByExampleSelective(@Param("record") VisitLogWithBLOBs record, @Param("example") VisitLogExample example);

    int updateByExampleWithBLOBs(@Param("record") VisitLogWithBLOBs record, @Param("example") VisitLogExample example);

    int updateByExample(@Param("record") VisitLog record, @Param("example") VisitLogExample example);
}