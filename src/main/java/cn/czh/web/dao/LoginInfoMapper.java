package cn.czh.web.dao;

import cn.czh.web.model.LoginInfo;
import cn.czh.web.model.LoginInfoExample;
import cn.czh.web.model.LoginInfoWithBLOBs;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface LoginInfoMapper {
    long countByExample(LoginInfoExample example);

    int deleteByExample(LoginInfoExample example);

    int insert(LoginInfoWithBLOBs record);

    int insertSelective(LoginInfoWithBLOBs record);

    List<LoginInfoWithBLOBs> selectByExampleWithBLOBs(LoginInfoExample example);

    List<LoginInfo> selectByExample(LoginInfoExample example);

    int updateByExampleSelective(@Param("record") LoginInfoWithBLOBs record, @Param("example") LoginInfoExample example);

    int updateByExampleWithBLOBs(@Param("record") LoginInfoWithBLOBs record, @Param("example") LoginInfoExample example);

    int updateByExample(@Param("record") LoginInfo record, @Param("example") LoginInfoExample example);
}