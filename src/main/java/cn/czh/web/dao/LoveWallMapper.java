package cn.czh.web.dao;

import cn.czh.web.model.LoveWall;
import cn.czh.web.model.LoveWallExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface LoveWallMapper {
    long countByExample(LoveWallExample example);

    int deleteByExample(LoveWallExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(LoveWall record);

    int insertSelective(LoveWall record);

    List<LoveWall> selectByExampleWithBLOBs(LoveWallExample example);

    List<LoveWall> selectByExample(LoveWallExample example);

    LoveWall selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") LoveWall record, @Param("example") LoveWallExample example);

    int updateByExampleWithBLOBs(@Param("record") LoveWall record, @Param("example") LoveWallExample example);

    int updateByExample(@Param("record") LoveWall record, @Param("example") LoveWallExample example);

    int updateByPrimaryKeySelective(LoveWall record);

    int updateByPrimaryKeyWithBLOBs(LoveWall record);

    int updateByPrimaryKey(LoveWall record);
}