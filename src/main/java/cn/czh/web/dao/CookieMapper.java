package cn.czh.web.dao;

import cn.czh.web.model.Cookie;
import cn.czh.web.model.CookieExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CookieMapper {
    long countByExample(CookieExample example);

    int deleteByExample(CookieExample example);

    int deleteByPrimaryKey(String user);

    int insert(Cookie record);

    int insertSelective(Cookie record);

    List<Cookie> selectByExample(CookieExample example);

    Cookie selectByPrimaryKey(String user);

    int updateByExampleSelective(@Param("record") Cookie record, @Param("example") CookieExample example);

    int updateByExample(@Param("record") Cookie record, @Param("example") CookieExample example);

    int updateByPrimaryKeySelective(Cookie record);

    int updateByPrimaryKey(Cookie record);
}