package cn.czh.web.dao;

import cn.czh.web.model.Jwtz;
import cn.czh.web.model.JwtzExample;
import cn.czh.web.model.JwtzWithBLOBs;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface JwtzMapper {
    long countByExample(JwtzExample example);

    int deleteByExample(JwtzExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(JwtzWithBLOBs record);

    int insertSelective(JwtzWithBLOBs record);

    List<JwtzWithBLOBs> selectByExampleWithBLOBs(JwtzExample example);

    List<Jwtz> selectByExample(JwtzExample example);

    JwtzWithBLOBs selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") JwtzWithBLOBs record, @Param("example") JwtzExample example);

    int updateByExampleWithBLOBs(@Param("record") JwtzWithBLOBs record, @Param("example") JwtzExample example);

    int updateByExample(@Param("record") Jwtz record, @Param("example") JwtzExample example);

    int updateByPrimaryKeySelective(JwtzWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(JwtzWithBLOBs record);

    int updateByPrimaryKey(Jwtz record);
}