package cn.czh.web.dao;

import cn.czh.web.model.XueSheng;
import cn.czh.web.model.XueShengExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface XueShengMapper {
    long countByExample(XueShengExample example);

    int deleteByExample(XueShengExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(XueSheng record);

    int insertSelective(XueSheng record);

    List<XueSheng> selectByExample(XueShengExample example);

    XueSheng selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") XueSheng record, @Param("example") XueShengExample example);

    int updateByExample(@Param("record") XueSheng record, @Param("example") XueShengExample example);

    int updateByPrimaryKeySelective(XueSheng record);

    int updateByPrimaryKey(XueSheng record);

    List<String> selectAllClass();
}