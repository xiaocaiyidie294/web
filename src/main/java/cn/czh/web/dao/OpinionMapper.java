package cn.czh.web.dao;

import cn.czh.web.model.Opinion;
import cn.czh.web.model.OpinionExample;
import cn.czh.web.model.OpinionWithBLOBs;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OpinionMapper {
    long countByExample(OpinionExample example);

    int deleteByExample(OpinionExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(OpinionWithBLOBs record);

    int insertSelective(OpinionWithBLOBs record);

    List<OpinionWithBLOBs> selectByExampleWithBLOBs(OpinionExample example);

    List<Opinion> selectByExample(OpinionExample example);

    OpinionWithBLOBs selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") OpinionWithBLOBs record, @Param("example") OpinionExample example);

    int updateByExampleWithBLOBs(@Param("record") OpinionWithBLOBs record, @Param("example") OpinionExample example);

    int updateByExample(@Param("record") Opinion record, @Param("example") OpinionExample example);

    int updateByPrimaryKeySelective(OpinionWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(OpinionWithBLOBs record);

    int updateByPrimaryKey(Opinion record);
}