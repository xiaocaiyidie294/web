package cn.czh.web.dao;

import cn.czh.web.model.BlackList;
import cn.czh.web.model.BlackListExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BlackListMapper {
    long countByExample(BlackListExample example);

    int deleteByExample(BlackListExample example);

    int insert(BlackList record);

    int insertSelective(BlackList record);

    List<BlackList> selectByExampleWithBLOBs(BlackListExample example);

    List<BlackList> selectByExample(BlackListExample example);

    int updateByExampleSelective(@Param("record") BlackList record, @Param("example") BlackListExample example);

    int updateByExampleWithBLOBs(@Param("record") BlackList record, @Param("example") BlackListExample example);

    int updateByExample(@Param("record") BlackList record, @Param("example") BlackListExample example);
}