package cn.czh.web.dao;


import cn.czh.web.model.CircleLike;
import cn.czh.web.model.CircleLikeExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CircleLikeMapper {
    long countByExample(CircleLikeExample example);

    int deleteByExample(CircleLikeExample example);

    int insert(CircleLike record);

    int insertSelective(CircleLike record);

    List<CircleLike> selectByExample(CircleLikeExample example);

    int updateByExampleSelective(@Param("record") CircleLike record, @Param("example") CircleLikeExample example);

    int updateByExample(@Param("record") CircleLike record, @Param("example") CircleLikeExample example);

    Map<String , Object> updateCircleLike(@Param("id") Integer id, @Param("username") String username);
}