package cn.czh.web.servlet;

import cn.czh.web.dao.BanJiMapper;
import cn.czh.web.dao.CircleCommentLikeMapper;
import cn.czh.web.util.AESUtil;
import cn.czh.web.util.TextUtil;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@WebServlet(
        name = "CircleCommentLike",
        urlPatterns = "/circleCommentLike"
)
public class CircleCommentLike extends BaseServlet{

    @Autowired
    private CircleCommentLikeMapper commentLikeMapper;

    @Override
    public boolean get(HttpServletRequest request, HttpServletResponse response, JSONObject result) {
        return false;
    }

    @Override
    public boolean post(HttpServletRequest request, HttpServletResponse response, JSONObject result) {
        String username = (String) request.getAttribute("username");
        if("guest".equals(username)) return false;
        String id = request.getParameter("id");

        if(!TextUtil.checkTextValid(id)) return false;

        int commentId = Integer.parseInt(id);

        Map<String , Object> map = commentLikeMapper.updateCircleCommentLike(commentId , username);
        System.out.println(map);
        if(map!=null && map.containsKey("result") && map.containsKey("count")){
            Long like = (Long) map.get("result");
            result.put("like" , like==1);
            result.put("count" , map.get("count"));
            result.put("id" ,commentId);
        }else{
            return false;
        }

        return true;
    }
}
