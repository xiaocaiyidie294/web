package cn.czh.web.servlet;

import cn.czh.web.dao.BanJiMapper;
import cn.czh.web.dao.XueShengMapper;
import cn.czh.web.model.XueSheng;
import cn.czh.web.model.XueShengExample;
import cn.czh.web.web.Application;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@WebServlet(
        name = "GetClass",
        urlPatterns = "/getClass"
)
public class GetClass extends BaseServlet {

    @Autowired
    public XueShengMapper xueShengMapper;

    @Override
    public boolean get(HttpServletRequest request, HttpServletResponse response, JSONObject result) {
        return false;
    }

    @Override
    public boolean post(HttpServletRequest request, HttpServletResponse response, JSONObject result) {
        String banji = request.getParameter("banji");
        String search = request.getParameter("search");

        if(banji!=null){
            XueShengExample example = new XueShengExample();
            example.createCriteria().andBjEqualTo(banji);
            List<XueSheng> list = xueShengMapper.selectByExample(example);
            result.put("array",list);
        }else if(search!=null){
            XueShengExample example = new XueShengExample();
            example.or().andNameLike('%' + search +  '%');
            example.or().andXhLike('%' + search + '%');
            example.setPageSize(200);
            example.setStartRow(0);
            List<XueSheng> list = xueShengMapper.selectByExample(example);
            result.put("array",list);
        }else{
            List<String> list = xueShengMapper.selectAllClass();
            result.put("array",list);
        }

        return true;
    }
}
