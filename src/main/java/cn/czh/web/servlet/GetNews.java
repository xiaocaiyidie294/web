package cn.czh.web.servlet;

import cn.czh.web.dao.NewsMapper;
import cn.czh.web.dao.UserMapper;
import cn.czh.web.model.News;
import cn.czh.web.model.NewsExample;
import cn.czh.web.web.Application;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@WebServlet(
        name = "GetNews",
        urlPatterns = "/getNews"
)
public class GetNews extends BaseServlet {

    @Autowired
    private NewsMapper newsMapper;

    @Override
    public boolean get(HttpServletRequest request, HttpServletResponse response, JSONObject result) {
        return false;
    }

    @Override
    public boolean post(HttpServletRequest request, HttpServletResponse response, JSONObject result) {
        int skip = Integer.parseInt(request.getParameter("offset"));

        int limit = 20;
        NewsExample example = new NewsExample();
        example.setPageSize(limit);
        example.setStartRow(skip);
        example.setOrderByClause("`time` desc");
//        if(search!=null){
//            example.createCriteria().andTitleLike("%" + search + "%");
//        }
        List<News> newsList = newsMapper.selectByExampleWithBLOBs(example);
        result.put("array",newsList);

        return true;
    }

}
