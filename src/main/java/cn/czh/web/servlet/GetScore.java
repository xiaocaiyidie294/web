package cn.czh.web.servlet;

import cn.czh.web.dao.EventMapper;
import cn.czh.web.dao.ScoreMapper;
import cn.czh.web.dao.UserMapper;
import cn.czh.web.model.EventExample;
import cn.czh.web.model.Score;
import cn.czh.web.model.ScoreExample;
import cn.czh.web.util.AESUtil;
import cn.czh.web.util.EventUtil;
import cn.czh.web.util.JwxtUtil;
import cn.czh.web.util.TextUtil;
import cn.czh.web.web.Application;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@WebServlet(
        name = "GetScore",
        urlPatterns = "/getScore"
)
public class GetScore extends BaseServlet {

    @Autowired
    private EventMapper eventMapper;
    @Autowired
    private ScoreMapper scoreMapper;
    @Autowired
    private UserMapper userMapper;

    @Override
    public boolean get(HttpServletRequest request, HttpServletResponse response, JSONObject result) {
        String username = (String) request.getAttribute("username");
        if("guest".equals(username)) return false;

        ScoreExample example = new ScoreExample();
        example.createCriteria().andSidEqualTo(username);
        List<Score> scores = scoreMapper.selectByExample(example);
        result.put("array",scores);

        EventExample eventExample = new EventExample();
        eventExample.createCriteria().andUserEqualTo(username).andNameEqualTo(EventUtil.GET_SCORE);
        eventExample.setOrderByClause("id desc");
        Date datetime = eventMapper.selectCurrentTimeByExample(eventExample);
        if(datetime != null){
            SimpleDateFormat format = new SimpleDateFormat("YYYY-MM-dd HH:mm");
            String time = format.format(datetime);
            result.put("time" , time);
        }

        return true;
    }

    @Override
    public boolean post(HttpServletRequest request, HttpServletResponse response, JSONObject result) {
        String token1 = request.getParameter("token1");
        String token2 = request.getParameter("token2");
        if (!TextUtil.checkTextValid(token1, token2)) return false;
        if(!token2.equals(DigestUtils.md5Hex(token1))) return false;

        String json = AESUtil.AESDecode(token1);
        String username = JSONObject.parseObject(json).getString("username");
        String password = userMapper.selectByPrimaryKey(username).getPassword();

        String cookie = JwxtUtil.getCookie(username,password);
        if(cookie==null){
            result.put("message","登录失败");
        }else {
            List<Score> scores = null;
            try {
                scores = JwxtUtil.getScores(username,cookie);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(scores!=null && scores.size()!=0){
                ScoreExample scoreExample = new ScoreExample();
                scoreExample.createCriteria().andSidEqualTo(username);
                scoreMapper.deleteByExample(scoreExample);
                for(Score score : scores){
                    scoreMapper.insert(score);
                }

                result.put("array",scores);
                result.put("time" , DateFormat.getDateTimeInstance().format(new Date()));
            }else{
                result.put("message","获取成绩失败");
            }

        }

        return true;
    }
}
