package cn.czh.web.servlet;

import cn.czh.web.web.COS;
import com.alibaba.fastjson.JSONObject;
import com.qcloud.cos.exception.CosClientException;
import com.qcloud.cos.exception.CosServiceException;
import com.qcloud.cos.model.ObjectMetadata;
import org.apache.commons.codec.binary.Base64;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.UUID;

@WebServlet(
        name = "ImageUpload",
        urlPatterns = "/image"
)
public class ImageUpload extends BaseServlet {
    @Override
    public boolean get(HttpServletRequest request, HttpServletResponse response, JSONObject result) {
        return false;
    }

    @Override
    public boolean post(HttpServletRequest request, HttpServletResponse response, JSONObject result) {
        String photo = request.getParameter("photo");

        String userAgent = request.getHeader("User-Agent");
        boolean isIOS = userAgent.contains("iPhone") || userAgent.contains("iPad");

        String uuid = UUID.randomUUID().toString();
        String format = getImageFormat(photo);
        byte[] bytes = Base64.decodeBase64(getImageBase64(photo));
        System.out.println(format);
        System.out.println(bytes.length);

        String key = uuid;
        System.out.println(key);
        try {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);

            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType("image/" + format);
            metadata.setContentLength(bytes.length);
            COS.getClient().putObject(COS.BUCKET_NAME , key , inputStream , metadata);

            if(isIOS){
                result.put("picUrl" , COS.getNormalJpg(COS.IMAGE_URL + key));
            }else{
                result.put("picUrl" , COS.getNormalWebp(COS.IMAGE_URL + key));
            }
            result.put("uuid" , uuid);

            inputStream.close();
        } catch (CosServiceException e){
            System.out.println("CosServiceException");
            System.out.println(e.getMessage());
        } catch (CosClientException e){
            System.out.println("CosClientException");
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println("IOException");
            System.out.println(e.getMessage());
        }

        System.out.println(result.toJSONString());
        return true;
    }

    public String getImageFormat(String image){
        String format;
        int start = image.indexOf('/') + 1;
        int end = image.indexOf(';');
        if(end-start>0){
            format = image.substring(start , end);
        }else{
            format = "webp";
        }
        return format;
    }

    public String getImageBase64(String image){
        int pos = image.indexOf(',');
        return image.substring(pos+1);
    }

}
