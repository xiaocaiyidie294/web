package cn.czh.web.servlet;

import cn.czh.web.dao.HeadMapper;
import cn.czh.web.dao.UserMapper;
import cn.czh.web.model.Head;
import cn.czh.web.model.HeadExample;
import cn.czh.web.util.AESUtil;
import cn.czh.web.util.Configuration;
import cn.czh.web.util.TextUtil;
import cn.czh.web.web.Application;
import cn.czh.web.web.COS;
import com.alibaba.fastjson.JSONObject;
import com.qcloud.cos.exception.CosClientException;
import com.qcloud.cos.exception.CosServiceException;
import com.qcloud.cos.model.ObjectMetadata;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@WebServlet(
        name = "HeadServlet",
        urlPatterns = "/head"
)
public class HeadServlet extends BaseServlet {

    @Autowired
    private HeadMapper headMapper;

    @Override
    public boolean get(HttpServletRequest request, HttpServletResponse response, JSONObject result) {
        String username = request.getParameter("username");

        String userAgent = request.getHeader("User-Agent");
        boolean isIOS = userAgent.contains("iPhone") || userAgent.contains("iPad");
        String url = COS.IMAGE_URL + "acaaa45a-9c3a-4bf6-a0aa-5173982d84d9";

        if (TextUtil.checkTextValid(username)) {
            Head head = headMapper.selectByPrimaryKey(username);
            if (head != null) {
                url = COS.IMAGE_URL + head.getUuid();
            }
        }

        if(isIOS){
            url = url.concat("!head+jpg");
        }else{
            url = url.concat("!head+webp");
        }

        response.setStatus(response.SC_MOVED_TEMPORARILY);
        response.setHeader("Location", url);

        return true;
    }

    @Override
    public boolean post(HttpServletRequest request, HttpServletResponse response, JSONObject result) {
        /*
        String username = (String) request.getAttribute("username");
        if("guest".equals(username)) return false;
        String photo = request.getParameter("photo");

        String userAgent = request.getHeader("User-Agent");
        boolean isIOS = userAgent.contains("iPhone") || userAgent.contains("iPad");

        String uuid = UUID.randomUUID().toString();
        String format = getImageFormat(photo);
        byte[] bytes = Base64.decodeBase64(getImageBase64(photo));
        System.out.println(format);
        System.out.println(bytes.length);

        if(bytes.length > Configuration.imageMaxLength) return false;

        String key = uuid;
        System.out.println(key);

        try {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);

            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType("image/" + format);
            metadata.setContentLength(bytes.length);
            COS.getClient().putObject(COS.BUCKET_NAME , key , inputStream , metadata);

            if(isIOS){
                result.put("picUrl" , COS.getHeadJpg(COS.IMAGE_URL + key));
            }else{
                result.put("picUrl" , COS.getHeadWebp(COS.IMAGE_URL + key));
            }
            result.put("uuid" , uuid);

            inputStream.close();
        } catch (CosServiceException e){
            System.out.println("CosServiceException");
            System.out.println(e.getMessage());
        } catch (CosClientException e){
            System.out.println("CosClientException");
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println("IOException");
            System.out.println(e.getMessage());
        }

        Head head = new Head();
        head.setUsername(username);
        head.setUuid(uuid);

        HeadExample headExample = new HeadExample();
        headExample.createCriteria().andUsernameEqualTo(username);
        if(headMapper.countByExample(headExample) > 0){
            headMapper.updateByPrimaryKey(head);
        }else{
            headMapper.insert(head);
        }
        */

        String username = (String) request.getAttribute("username");
        if("guest".equals(username)) return false;

        //设置工厂
        DiskFileItemFactory factory = new DiskFileItemFactory();
        //设置缓冲区大小5M
        factory.setSizeThreshold(1024 * 1024 * 5);
        //设置临时文件
        factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
        //设置解析器
        ServletFileUpload sUpload = new ServletFileUpload(factory);
        //最大文件尺寸,4MB
        sUpload.setFileSizeMax(5 * 1024 * 1024);

        String uuid = null;

        //解析结果放list
        try {
            List<FileItem> list = sUpload.parseRequest(request);
            System.out.println("表单数据项数：" + list.size());
            for (FileItem item : list) {
                String name = item.getFieldName();
                System.out.println("数据项名：" + name);

                if(name.startsWith("images")){
                    uuid = UUID.randomUUID().toString();

                    ObjectMetadata metadata = new ObjectMetadata();
                    metadata.setContentType(item.getContentType());
                    metadata.setContentLength(item.getSize());
                    COS.getClient().putObject(COS.BUCKET_NAME , uuid , item.getInputStream() ,metadata);
                }
            }
        } catch (FileUploadException | IOException e) {
            e.printStackTrace();
            return false;
        }

        if(uuid != null){
            Head head = new Head();
            head.setUsername(username);
            head.setUuid(uuid);

            HeadExample headExample = new HeadExample();
            headExample.createCriteria().andUsernameEqualTo(username);
            if(headMapper.countByExample(headExample) > 0){
                headMapper.updateByPrimaryKey(head);
            }else{
                headMapper.insert(head);
            }
        }

        return true;
    }

    public String getImageFormat(String image){
        String format;
        int start = image.indexOf('/') + 1;
        int end = image.indexOf(';');
        if(end-start>0){
            format = image.substring(start , end);
        }else{
            format = "webp";
        }
        return format;
    }

    public String getImageBase64(String image){
        int pos = image.indexOf(',');
        return image.substring(pos+1);
    }
}
