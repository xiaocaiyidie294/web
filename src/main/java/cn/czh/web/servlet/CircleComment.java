package cn.czh.web.servlet;

import cn.czh.web.dao.BanJiMapper;
import cn.czh.web.dao.CircleCommentMapper;
import cn.czh.web.model.CircleCommentExample;
import cn.czh.web.model.CircleCommentModel;
import cn.czh.web.util.AESUtil;
import cn.czh.web.util.TextUtil;
import cn.czh.web.web.COS;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.util.TextUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@WebServlet(
        name = "CircleComment",
        urlPatterns = "/circleComment"
)
public class CircleComment extends BaseServlet {

    @Autowired
    private CircleCommentMapper commentMapper;

    @Override
    public boolean get(HttpServletRequest request, HttpServletResponse response, JSONObject result) {
        String strId = request.getParameter("id");
        String strLimit = request.getParameter("limit");
        String strOffset = request.getParameter("offset");
        String username = (String) request.getAttribute("username");
        if(!TextUtil.checkTextValid(strId , strLimit , strOffset)) return false;

        String userAgent = request.getHeader("User-Agent");
        boolean isIOS = userAgent.contains("iPhone") || userAgent.contains("iPad");

        int id = Integer.parseInt(strId);
        int limit = Integer.parseInt(strLimit);
        int skip = Integer.parseInt(strOffset);

        CircleCommentExample example = new CircleCommentExample();
        example.setPageSize(limit);
        example.setStartRow(skip);
        example.setUsername(username);
        example.setOrderByClause("`comment_id` desc");
        example.createCriteria().andCircleIdEqualTo(id);
        List<CircleCommentModel> comments = commentMapper.selectModelByExampleWithBLOBs(example);

        for (CircleCommentModel model : comments) {
            if(model.getHead() != null){
                if(isIOS){
                    model.setHead(COS.getHeadJpg(COS.getImageUrl(model.getHead())));
                }else{
                    model.setHead(COS.getHeadWebp(COS.getImageUrl(model.getHead())));
                }
            }else{
                if(isIOS){
                    model.setHead(COS.getHeadJpg(COS.getImageUrl(COS.DEFAULT_HEAD_UUID)));
                }else{
                    model.setHead(COS.getHeadWebp(COS.getImageUrl(COS.DEFAULT_HEAD_UUID)));
                }
            }
        }

        result.put("array" , comments);
        return true;
    }

    @Override
    public boolean post(HttpServletRequest request, HttpServletResponse response, JSONObject result) {
        String content = request.getParameter("content");
        Integer circle_id = Integer.parseInt(request.getParameter("circleId"));
        String answer_id = request.getParameter("answerId");
        String username = (String) request.getAttribute("username");
        if("guest".equals(username)) return false;

        if(!TextUtil.checkTextValid(content)) return false;
        if(content.length() > 200) return false;

        System.out.println(answer_id);
        System.out.println(circle_id);
        System.out.println(content);

        Integer answerId = null;
        if(!TextUtils.isEmpty(answer_id) && answer_id.matches("\\d+")){
            answerId = Integer.parseInt(answer_id);
        }

        commentMapper.insertCircleComment(circle_id , username , answerId , content);

        result.put("success" , true);
        return true;
    }
}
