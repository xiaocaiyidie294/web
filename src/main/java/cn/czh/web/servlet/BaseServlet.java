package cn.czh.web.servlet;

import cn.czh.web.service.RedisService;
import cn.czh.web.util.TextUtil;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract  class BaseServlet extends HttpServlet {

    public abstract boolean get(HttpServletRequest request, HttpServletResponse response , JSONObject result);

    public abstract boolean post(HttpServletRequest request, HttpServletResponse response , JSONObject result);

    @Autowired
    public RedisService redisService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        JSONObject result = new JSONObject();

        request.setAttribute("username",getUser(request));

        if(!get(request , response , result)) return;

//        response.setContentType("text/json;charset=UTF-8");
        response.setContentType("application/json;charset=UTF-8");
        response.setCharacterEncoding("utf-8");
//        response.setHeader("content-encoding","gzip");
        response.setHeader("Access-Control-Allow-Origin", "*");

        request.setAttribute("result",result.toJSONString());
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        JSONObject result = new JSONObject();

        request.setAttribute("username",getUser(request));

        if(!post(request , response , result))return;

//        response.setContentType("text/json; charset=UTF-8");
        response.setContentType("application/json;charset=UTF-8");
        response.setCharacterEncoding("utf-8");
//        response.setHeader("content-encoding","gzip");
        response.setHeader("Access-Control-Allow-Origin", "*");

        request.setAttribute("result",result.toJSONString());
    }


    private String getUser(HttpServletRequest request){
        String username = "guest";
        String token1 = request.getParameter("token1");;
        String token2 = request.getParameter("token2");;
        if(!TextUtil.checkTextValid(token1,token2)) return username;
        if(redisService.containsKey(redisService.keyname("token2",token2))){
            if(redisService.containsKey(redisService.keyname("token1",token1))){
                username = (String) redisService.get(redisService.keyname("token1",token1));
            }
        }

        return username;
    }
}
