package cn.czh.web.servlet;

import cn.czh.web.dao.UserMapper;
import cn.czh.web.model.User;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(
        name = "GetUserInfo",
        urlPatterns = "/getUserInfo"
)
public class GetUserInfo extends BaseServlet {

    @Autowired
    private UserMapper userMapper;

    @Override
    public boolean get(HttpServletRequest request, HttpServletResponse response, JSONObject result) {
        String username = request.getParameter("username");

        User user = userMapper.selectByPrimaryKey(username);

        if(user != null){
            result.put("nickname",user.getNickname());
            result.put("banji" , user.getBanji());
        }


        return true;
    }

    @Override
    public boolean post(HttpServletRequest request, HttpServletResponse response, JSONObject result) {
        String username = (String) request.getAttribute("username");
        if("guest".equals(username)) return false;

        User user = userMapper.selectByPrimaryKey(username);

        if(user==null) return false;

        result.put("username",user.getUsername());
//        result.put("name",user.getName());
        result.put("banji",user.getBanji());
//        result.put("sex" , user.getSex());
//        result.put("tel" , user.getTel());
        result.put("nickname" , user.getNickname());
//        result.put("remark" , user.getRemark());
//        result.put("createAt" , user.getCreateTime());
//        result.put("updateAt" , user.getUpdateTime());

        return true;
    }
}
