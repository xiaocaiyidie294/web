package cn.czh.web.servlet;

import cn.czh.web.dao.BanJiMapper;
import cn.czh.web.dao.CircleImageMapper;
import cn.czh.web.dao.CircleMapper;
import cn.czh.web.model.Circle;
import cn.czh.web.model.CircleExample;
import cn.czh.web.model.CircleModel;
import cn.czh.web.model.CircleImage;
import cn.czh.web.util.AESUtil;
import cn.czh.web.util.TextUtil;
import cn.czh.web.web.Application;
import cn.czh.web.web.COS;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@WebServlet(
        name = "CircleServlet",
        urlPatterns = "/circle"
)
public class CircleServlet extends BaseServlet {

    @Autowired
    private CircleMapper circleMapper;
    @Autowired
    private CircleImageMapper circleImageMapper;

    @Override
    public boolean get(HttpServletRequest request, HttpServletResponse response, JSONObject result) {
        int limit = 10;
        Integer offset = Integer.parseInt(request.getParameter("offset"));

        String userAgent = request.getHeader("User-Agent");
        boolean isIOS = userAgent.contains("iPhone") || userAgent.contains("iPad");

        String username = (String) request.getAttribute("username");

        CircleExample example = new CircleExample();
        example.setPageSize(limit);
        example.setUsername(username);
        example.setOrderByClause("`circle_id` desc");
        if(offset>0){
            example.createCriteria().andCircleIdGreaterThan(offset);
        }
        List<CircleModel> circles = circleMapper.selectModelByExampleWithBLOBs(example);


        for (CircleModel model : circles) {
            if(model.getImages() != null){
                String[] urls = model.getImages().split(" ");
                for (int i = 0; i < urls.length; i++) {
                    if(isIOS){
                        urls[i] = COS.getSmallJpg(COS.getImageUrl(urls[i]));
                    }else{
                        urls[i] = COS.getSmallWebp(COS.getImageUrl(urls[i]));
                    }
                }
                model.setUrls(urls);
                model.setImages(null);
            }

            if(model.getHead() != null){
                if(isIOS){
                    model.setHead(COS.getHeadJpg(COS.getImageUrl(model.getHead())));
                }else{
                    model.setHead(COS.getHeadWebp(COS.getImageUrl(model.getHead())));
                }
            }else{
                if(isIOS){
                    model.setHead(COS.getHeadJpg(COS.getImageUrl(COS.DEFAULT_HEAD_UUID)));
                }else{
                    model.setHead(COS.getHeadWebp(COS.getImageUrl(COS.DEFAULT_HEAD_UUID)));
                }
            }
        }

        result.put("array",circles);
        return true;
    }

    @Override
    public boolean post(HttpServletRequest request, HttpServletResponse response, JSONObject result) {
        String content = request.getParameter("content");
        String uuids = request.getParameter("uuids");

        String username = (String) request.getAttribute("username");
        if("guest".equals(username)) return false;

        List<String> uuidList = JSONArray.parseArray(uuids).toJavaList(String.class);

        Circle circle = new Circle();
        circle.setUsername(username);
        circle.setContent(content);
        circleMapper.insert(circle);

        for (String uuid : uuidList){
            CircleImage circleImage = new CircleImage();
            circleImage.setCircleId(circle.getCircleId());
            circleImage.setUuid(uuid);
            circleImageMapper.insert(circleImage);
        }

        return true;
    }

}
