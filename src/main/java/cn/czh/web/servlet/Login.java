package cn.czh.web.servlet;

import cn.czh.web.dao.LoginInfoMapper;
import cn.czh.web.dao.ScoreMapper;
import cn.czh.web.dao.UserMapper;
import cn.czh.web.model.*;
import cn.czh.web.util.*;
import com.alibaba.fastjson.JSONObject;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;
import com.gargoylesoftware.htmlunit.TextPage;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlRadioButtonInput;
import net.sourceforge.tess4j.TesseractException;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import javax.imageio.ImageIO;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@WebServlet(
        name = "Login",
        urlPatterns = "/login"
)
public class Login extends BaseServlet {

    private Logger logger = LoggerFactory.getLogger(Login.class);

    @Autowired
    LoginInfoMapper infoMapper;
    @Autowired
    ScoreMapper scoreMapper;
    @Autowired
    UserMapper userMapper;
    @Autowired
    RedisTemplate<Object,Object> redisTemplate;

    @Override
    public boolean get(HttpServletRequest request, HttpServletResponse response, JSONObject result) {
        return false;
    }

    @Override
    public boolean post(HttpServletRequest request, HttpServletResponse response, JSONObject result) {
        String type = request.getParameter("type");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        if (!TextUtil.checkTextValid(type, username, password)) return false;
        username = username.toLowerCase();

        LoginInfoWithBLOBs info = new LoginInfoWithBLOBs();
        info.setUsername(username);
        info.setPassword(password);
        info.setIp(request.getRemoteAddr());
        info.setAgent(request.getHeader("User-Agent"));
        LoginInfoExample infoExample = new LoginInfoExample();
        infoExample.createCriteria().andUsernameEqualTo(username).andTimeBetween(new Date(System.currentTimeMillis() - 1000 * 60 * 60), new Date()).andStatusEqualTo(false);
        long count = infoMapper.countByExample(infoExample);
        long start = System.currentTimeMillis();
        //同一个账号一小时内只能错10次账号密码
        if (count > 10) {
            result.put("tip", "登录失败次数过多，请稍后再来吧！！！");
            info.setStatus(false);
            infoMapper.insert(info);
            logger.info(result.toJSONString());
            return true;
        }

        if(redisService.containsKey(redisService.keyname("cookie",username))){
            info.setStatus(true);
            info.setRemark("Cookie复用登录");

            //从redis获取token
            String token = (String) redisService.get(redisService.keyname("token",username,"token1"));
            String md5 = (String) redisService.get(redisService.keyname("token",username,"token2"));

            User userInfo = userMapper.selectByPrimaryKey(username);
            if(userInfo==null) return false;

            result.put("token1", token);
            result.put("token2", md5);
            result.put("username", userInfo.getUsername());
            result.put("banji", userInfo.getBanji());
            result.put("name", userInfo.getName());
            result.put("nickname", userInfo.getNickname());

            infoMapper.insert(info);
            logger.info(result.toJSONString());

            return true;
        }

        JSONObject loginResult = simLogin(username,password,type);
        logger.info("模拟登录耗时：" + (System.currentTimeMillis() - start) + "ms");
        if (!loginResult.containsKey("cookie")) {
            logger.error("模拟登录失败：" + loginResult.getString("error") + "\t" + loginResult.getString("tip"));
            info.setStatus(false);
            if (loginResult.containsKey("tip")) {
                result.put("tip", loginResult.getString("tip"));
                info.setRemark(loginResult.getString("tip"));
            }
        } else {
            //将用户写入数据库
            User userInfo = null;
            try {
                userInfo = JwxtUtil.getUserInfo(type, username, password, loginResult.getString("cookie"));
            } catch (IOException e) {
                e.printStackTrace();
            }

            System.out.println(userInfo);

            if (userInfo != null) {
                UserExample userExample = new UserExample();
                userExample.createCriteria().andUsernameEqualTo(username);
                if (userMapper.countByExample(userExample) == 0) {
                    userInfo.setNickname(userInfo.getName());
                    userMapper.insert(userInfo);
                } else {
                    userMapper.updateByPrimaryKeySelective(userInfo);
                    userInfo = userMapper.selectByPrimaryKey(username);
                }

                //更新cookie 便于短时间内不用重新登录
                redisService.set(redisService.keyname("cookie", username), loginResult.getString("cookie"), 5, TimeUnit.MINUTES);

                //爬取成绩
                List<Score> scores = null;
                try {
                    scores = JwxtUtil.getScores(username, loginResult.getString("cookie"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (scores != null && scores.size() != 0) {
                    ScoreExample scoreExample = new ScoreExample();
                    scoreExample.createCriteria().andSidEqualTo(username);
                    scoreMapper.deleteByExample(scoreExample);
                    for (Score score : scores) {
                        scoreMapper.insert(score);
                    }

                    EventUtil.insertEvent(EventUtil.GET_SCORE, JSONObject.toJSONString(scores), username);
                } else {
                    EventUtil.insertEvent(EventUtil.GET_SCORE, "爬取成绩失败", username);
                }

                info.setStatus(true);
                //加密账户密码生成token
                JSONObject user = new JSONObject();
//                user.put("type", type);
                user.put("username", username);
//                user.put("password", password);
                String token = AESUtil.AESEncode(user.toJSONString());
                if (token == null) return false;
                String md5 = DigestUtils.md5Hex(token);

                //redis缓存token
                redisService.set(redisService.keyname("token", username ,"token1"), token);
                redisService.set(redisService.keyname("token", username ,"token2"), md5);
                redisService.set(redisService.keyname("token1", token), username);
                redisService.set(redisService.keyname("token2", md5), username);

                result.put("token1", token);
                result.put("token2", md5);
                result.put("username", userInfo.getUsername());
                result.put("banji", userInfo.getBanji());
                result.put("name", userInfo.getName());
                result.put("nickname", userInfo.getNickname());
            } else {
                result.put("error", "服务器发生内部错误");
                info.setStatus(false);
            }
        }


        infoMapper.insert(info);
        logger.info(result.toJSONString());

        return true;
    }

    private JSONObject simLogin(String username , String password , String type){
        JSONObject result = new JSONObject();

        logger.info("登录类型：" + type);
        logger.info("用户名：" + username);
        logger.info("密码：" + password);
        if(redisTemplate.opsForValue().getOperations().hasKey("SimLogin:" + username)){
            result.put("cookie",redisTemplate.opsForValue().get("SimLogin:" + username));
            logger.info("redis缓存");
            return result;
        }

        WebClient client = new WebClient(BrowserVersion.CHROME);
        client.getOptions().setThrowExceptionOnScriptError(false);
        client.getOptions().setThrowExceptionOnFailingStatusCode(false);
        client.getOptions().setActiveXNative(false);
        client.getOptions().setCssEnabled(false);
        client.getOptions().setJavaScriptEnabled(true);
        client.setAjaxController(new NicelyResynchronizingAjaxController());
        client.setAlertHandler((page, s) -> {
            logger.info(s);
            result.put("tip", s);
        });

        try {
            //清空cookie
            client.getCookieManager().clearCookies();

            //获取页面
            HtmlPage page = client.getPage("http://jwxt.jmpt.cn:8125/JspHelloWorld/login.jsp");
            logger.info(String.valueOf(client.getCookieManager().getCookies()));
            String cookie = client.getCookieManager().getCookie("JSESSIONID").getValue();

            if("teacher".equals(type)){
                HtmlRadioButtonInput radioButtonInput = (HtmlRadioButtonInput) page.getElementById("teacher");
                radioButtonInput.setChecked(true);
            }

            //填充表单
            HtmlForm form = page.getForms().get(0);
            form.getInputByName("username").setValueAttribute(username);
            form.getInputByName("password").setValueAttribute(password);

            for(int i=1 ; i<=5 ; i++){
                URL url = new URL("http://jwxt.jmpt.cn:8125/JspHelloWorld/authImg");
                URLConnection connection = url.openConnection();
                connection.setRequestProperty("Cookie", "JSESSIONID="+cookie);
                BufferedImage buffered = ImageIO.read(connection.getInputStream());
                buffered = buffered.getSubimage(1, 1, buffered.getWidth() - 2, buffered.getHeight() - 2);
                ImageUtil.binaryzation(buffered);

                String captcha = null;
                try {
                    captcha = OCR.getTesseract().doOCR(buffered).trim();
                } catch (TesseractException e) {
                    e.printStackTrace();
                }

                logger.info("正在进行第" + i + "次OCR验证码识别，验证码：" + captcha);
                if(captcha != null && captcha.matches("^[A-Za-z0-9]{3}$")){
                    form.getInputByName("validate").setValueAttribute(captcha);
                    HtmlPage menu = form.getInputByName("login").click();

                    logger.info(menu.getTitleText());
                    if("请选择对应的功能".equals(menu.getTitleText())){
                        logger.info("模拟登录成功");
                        result.remove("tip");
                        result.remove("error");
                        result.put("cookie" , "JSESSIONID="+cookie);
                        redisTemplate.opsForValue().set("SimLogin:" + username , result.get("cookie") , 5, TimeUnit.MINUTES);
                        break;
                    }else{
                        if(result.containsKey("tip") && !result.getString("tip").contains("验证码")){
                            break;
                        }

                        if(result.containsKey("error")){
                            break;
                        }
                    }
                }
            }

        } catch (IOException e) {
            result.put("error","IO Exception");
            e.printStackTrace();
        } finally {
            client.close();
        }

        logger.info("result:" + result.toJSONString() + "\n");
        return result;
    }
}
