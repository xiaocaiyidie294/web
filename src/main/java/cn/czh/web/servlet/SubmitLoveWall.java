package cn.czh.web.servlet;

import cn.czh.web.dao.LoveWallMapper;
import cn.czh.web.dao.UserMapper;
import cn.czh.web.model.LoveWall;
import cn.czh.web.util.AESUtil;
import cn.czh.web.util.TextUtil;
import cn.czh.web.web.Application;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(
        name = "SubmitLoveWall",
        urlPatterns = "/submitLoveWall"
)
public class SubmitLoveWall extends BaseServlet {

    @Autowired
    private LoveWallMapper loveWallMapper;

    @Override
    public boolean get(HttpServletRequest request, HttpServletResponse response, JSONObject result) {
        return false;
    }

    @Override
    public boolean post(HttpServletRequest request, HttpServletResponse response, JSONObject result) {
        String target = request.getParameter("target");
        String text = request.getParameter("text");
        String username = (String) request.getAttribute("username");
        if("guest".equals(username)) return false;
        if(!TextUtil.checkTextValid(text)) return false;

        if(target.length()==0){
            target = "所有人";
        }

        LoveWall loveWall = new LoveWall();
        loveWall.setTarget(target);
        loveWall.setText(text);
        loveWall.setUsername(username);
        loveWallMapper.insert(loveWall);

        return true;
    }
}
