package cn.czh.web.servlet;

import cn.czh.web.dao.BanJiMapper;
import cn.czh.web.dao.XueShengMapper;
import cn.czh.web.web.Application;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@WebServlet(name = "GetAllClass" ,urlPatterns = "/getAllClass")
public class GetAllClass extends BaseServlet {

    @Autowired
    private XueShengMapper xueShengMapper;

    @Override
    public boolean get(HttpServletRequest request, HttpServletResponse response, JSONObject result) {
        List<String> list = xueShengMapper.selectAllClass();
        result.put("array",list);
        return true;
    }

    @Override
    public boolean post(HttpServletRequest request, HttpServletResponse response, JSONObject result) {
        return false;
    }
}
