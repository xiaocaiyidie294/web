package cn.czh.web.servlet;

import cn.czh.web.web.COS;
import com.qcloud.cos.model.ObjectMetadata;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@WebServlet(
        name = "CircleFile",
        urlPatterns = "/circleFile"
)

public class CircleFile extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        //设置工厂
        DiskFileItemFactory factory = new DiskFileItemFactory();
        //设置缓冲区大小5M
        factory.setSizeThreshold(1024 * 1024 * 5);
        //设置临时文件
        factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
        //设置解析器
        ServletFileUpload sUpload = new ServletFileUpload(factory);
        //最大文件尺寸,4MB
        sUpload.setFileSizeMax(5 * 1024 * 1024);

        String uuid = null;

        //解析结果放list
        try {
            List<FileItem> list = sUpload.parseRequest(request);
            System.out.println("表单数据项数：" + list.size());
            for (FileItem item : list) {
                String name = item.getFieldName();
                System.out.println("数据项名：" + name);

                if(name.startsWith("images")){
                    uuid = UUID.randomUUID().toString();

                    ObjectMetadata metadata = new ObjectMetadata();
                    metadata.setContentType(item.getContentType());
                    metadata.setContentLength(item.getSize());
                    COS.getClient().putObject(COS.BUCKET_NAME , uuid , item.getInputStream() ,metadata);
                }
            }
        } catch (FileUploadException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(uuid != null){
            try {
                response.getWriter().write(uuid);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
