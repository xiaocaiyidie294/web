package cn.czh.web.servlet;

import cn.czh.web.dao.BanJiMapper;
import cn.czh.web.dao.CourseMapper;
import cn.czh.web.dao.UserMapper;
import cn.czh.web.model.Course;
import cn.czh.web.model.CourseExample;
import cn.czh.web.model.User;
import cn.czh.web.util.AESUtil;
import cn.czh.web.util.TextUtil;
import cn.czh.web.web.Application;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@WebServlet(name = "GetCourse" ,urlPatterns = "/getCourse")
public class GetCourse extends BaseServlet {

    @Autowired
    private CourseMapper courseMapper;
    @Autowired
    private UserMapper userMapper;

    @Override
    public boolean get(HttpServletRequest request, HttpServletResponse response, JSONObject result) {
        return false;
    }

    @Override
    public boolean post(HttpServletRequest request, HttpServletResponse response, JSONObject result) {
        String time = request.getParameter("time");
        String banji = request.getParameter("banji");
        String username = (String) request.getAttribute("username");


        Date date = new Date(Long.parseLong(time));
        Calendar startDate = Calendar.getInstance();
        startDate.setTime(date);
        startDate.set(Calendar.HOUR_OF_DAY , 0);
        startDate.set(Calendar.MINUTE , 0);
        startDate.set(Calendar.SECOND , 0);
        Calendar endDate = (Calendar) startDate.clone();
        endDate.add(Calendar.DAY_OF_YEAR , 1);

        if(banji==null && "guest".equals(username)){
            return false;
        }



        List<Course> list = new ArrayList();
        if(banji==null){
            User user = userMapper.selectByPrimaryKey(username);
            CourseExample example = new CourseExample();
            example.createCriteria().andBanjiEqualTo(user.getBanji()).andStartBetween(startDate.getTime(),endDate.getTime());
            list.addAll(courseMapper.selectByExample(example));
        }else{
            CourseExample example = new CourseExample();
            example.createCriteria().andBanjiEqualTo(banji).andStartBetween(startDate.getTime(),endDate.getTime());
            list.addAll(courseMapper.selectByExample(example));
        }

        result.put("array" , list);

        return true;
    }
}
