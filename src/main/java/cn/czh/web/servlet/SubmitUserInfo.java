package cn.czh.web.servlet;

import cn.czh.web.dao.UserMapper;
import cn.czh.web.model.User;
import cn.czh.web.util.AESUtil;
import cn.czh.web.util.TextUtil;
import cn.czh.web.web.Application;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

@WebServlet(
        name = "SubmitUserInfo",
        urlPatterns = "/submitUserInfo"
)
public class SubmitUserInfo extends BaseServlet {

    @Autowired
    private UserMapper userMapper;

    @Override
    public boolean get(HttpServletRequest request, HttpServletResponse response, JSONObject result) {
        return false;
    }

    @Override
    public boolean post(HttpServletRequest request, HttpServletResponse response, JSONObject result) {
        String nickname = request.getParameter("nickname");
        String remark = request.getParameter("remark");

        String username = (String) request.getAttribute("username");
        if("guest".equals(username)) return false;

        if(nickname!=null && nickname.length()>0 && nickname.length()<=10){
            User user = new User();
            user.setUsername(username);
            user.setNickname(nickname);
            user.setUpdateTime(new Date());
            userMapper.updateByPrimaryKeySelective(user);
            result.put("nickname" , nickname);
            return true;
        }

        if(remark!=null && remark.length()>0 && remark.length()<=100){
            User user = new User();
            user.setUsername(username);
            user.setRemark(remark);
            userMapper.updateByPrimaryKeySelective(user);
            result.put("remark" , remark);
            return true;
        }

        return false;
    }

}
