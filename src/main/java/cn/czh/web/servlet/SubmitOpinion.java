package cn.czh.web.servlet;

import cn.czh.web.dao.OpinionMapper;
import cn.czh.web.dao.UserMapper;
import cn.czh.web.model.OpinionWithBLOBs;
import cn.czh.web.util.AESUtil;
import cn.czh.web.util.TextUtil;
import cn.czh.web.web.Application;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(
        name = "SubmitOpinion",
        urlPatterns = "/submitOpinion"
)
public class SubmitOpinion extends BaseServlet {

    @Autowired
    private OpinionMapper opinionMapper;

    @Override
    public boolean get(HttpServletRequest request, HttpServletResponse response, JSONObject result) {
        return false;
    }

    @Override
    public boolean post(HttpServletRequest request, HttpServletResponse response, JSONObject result) {
        String content = request.getParameter("content");
        String uuids = request.getParameter("uuids");
        String contact = request.getParameter("contact");
        int star = Integer.parseInt(request.getParameter("star"));
        String username = (String) request.getAttribute("username");
        if("guest".equals(username)) return false;

        if (!TextUtil.checkTextValid(content)) return false;
        if(star<=0 || star>5) return false;

        OpinionWithBLOBs opinion = new OpinionWithBLOBs();
        opinion.setXh(username);
        opinion.setText(content);
        opinion.setContact(contact);
        opinion.setUuids(uuids);
        opinion.setStar(star);
        opinionMapper.insert(opinion);

        return true;
    }

}
