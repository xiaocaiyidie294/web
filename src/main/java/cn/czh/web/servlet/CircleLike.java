package cn.czh.web.servlet;

import cn.czh.web.dao.CircleLikeMapper;
import cn.czh.web.util.TextUtil;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Enumeration;
import java.util.Map;

@WebServlet(
        name = "CircleLike",
        urlPatterns = "/circleLike"
)
public class CircleLike extends BaseServlet{

    @Autowired
    private CircleLikeMapper circleLikeMapper;

    Logger logger = LoggerFactory.getLogger(CircleLike.class);

    @Override
    public boolean get(HttpServletRequest request, HttpServletResponse response, JSONObject result) {
        return false;
    }

    @Override
    public boolean post(HttpServletRequest request, HttpServletResponse response, JSONObject result) {

        String id = request.getParameter("id");
        String username = (String) request.getAttribute("username");
        if("guest".equals(username)) return false;
        if(!TextUtil.checkTextValid(id)) return false;

        int circle_id = Integer.parseInt(id);
        logger.info(username);

        Map<String , Object> map = circleLikeMapper.updateCircleLike(circle_id , username);
        if(map != null && map.containsKey("result") && map.containsKey("count")){
            Long like = (Long) map.get("result");
            result.put("like" , like==1);
            result.put("count" , map.get("count"));
            result.put("id" ,circle_id);
        }else{
            return false;
        }

        return true;
    }
}
