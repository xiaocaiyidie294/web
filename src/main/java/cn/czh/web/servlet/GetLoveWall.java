package cn.czh.web.servlet;

import cn.czh.web.dao.LoveWallMapper;
import cn.czh.web.dao.UserMapper;
import cn.czh.web.model.LoveWall;
import cn.czh.web.model.LoveWallExample;
import cn.czh.web.web.Application;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@WebServlet(
        name = "GetLoveWall",
        urlPatterns = "/getLoveWall"
)
public class GetLoveWall extends BaseServlet {

    @Autowired
    private LoveWallMapper loveWallMapper;

    @Override
    public boolean get(HttpServletRequest request, HttpServletResponse response, JSONObject result) {
        return false;
    }

    @Override
    public boolean post(HttpServletRequest request, HttpServletResponse response, JSONObject result) {
        int limit = 20;
        Integer offset = Integer.parseInt(request.getParameter("offset"));

        LoveWallExample example = new LoveWallExample();
        example.setPageSize(limit);
        example.setOrderByClause("`id` desc");
        if(offset>0){
            example.createCriteria().andIdLessThan(offset);
        }
        List<LoveWall> walls = loveWallMapper.selectByExampleWithBLOBs(example);

        for (LoveWall wall : walls) {
            wall.setUsername(null);
        }
        
        result.put("array",walls);

        return true;
    }
}
