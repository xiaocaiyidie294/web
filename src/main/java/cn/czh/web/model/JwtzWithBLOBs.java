package cn.czh.web.model;

public class JwtzWithBLOBs extends Jwtz {
    private String content;

    private String url;

    public JwtzWithBLOBs(Integer id, String title, String time, String department, String term, String content, String url) {
        super(id, title, time, department, term);
        this.content = content;
        this.url = url;
    }

    public JwtzWithBLOBs() {
        super();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}