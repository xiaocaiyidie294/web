package cn.czh.web.model;

import java.util.Date;

public class CircleCommentLike {
    private Integer commentId;

    private String username;

    private Date time;

    public CircleCommentLike(Integer commentId, String username, Date time) {
        this.commentId = commentId;
        this.username = username;
        this.time = time;
    }

    public CircleCommentLike() {
        super();
    }

    public Integer getCommentId() {
        return commentId;
    }

    public void setCommentId(Integer commentId) {
        this.commentId = commentId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}