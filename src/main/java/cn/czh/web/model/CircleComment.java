package cn.czh.web.model;

import java.util.Date;

public class CircleComment {
    private Integer commentId;

    private Integer circleId;

    private String username;

    private Integer answer;

    private Date time;

    private String content;

    public CircleComment(Integer commentId, Integer circleId, String username, Integer answer, Date time) {
        this.commentId = commentId;
        this.circleId = circleId;
        this.username = username;
        this.answer = answer;
        this.time = time;
    }

    public CircleComment(Integer commentId, Integer circleId, String username, Integer answer, Date time, String content) {
        this.commentId = commentId;
        this.circleId = circleId;
        this.username = username;
        this.answer = answer;
        this.time = time;
        this.content = content;
    }

    public CircleComment() {
        super();
    }

    public Integer getCommentId() {
        return commentId;
    }

    public void setCommentId(Integer commentId) {
        this.commentId = commentId;
    }

    public Integer getCircleId() {
        return circleId;
    }

    public void setCircleId(Integer circleId) {
        this.circleId = circleId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getAnswer() {
        return answer;
    }

    public void setAnswer(Integer answer) {
        this.answer = answer;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}