package cn.czh.web.model;

import java.util.Date;

public class VisitLog {
    private Integer id;

    private String url;

    private String type;

    private Short code;

    private String ip;

    private String username;

    private Short spend;

    private Date time;

    public VisitLog(Integer id, String url, String type, Short code, String ip, String username, Short spend, Date time) {
        this.id = id;
        this.url = url;
        this.type = type;
        this.code = code;
        this.ip = ip;
        this.username = username;
        this.spend = spend;
        this.time = time;
    }

    public VisitLog() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Short getCode() {
        return code;
    }

    public void setCode(Short code) {
        this.code = code;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Short getSpend() {
        return spend;
    }

    public void setSpend(Short spend) {
        this.spend = spend;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}