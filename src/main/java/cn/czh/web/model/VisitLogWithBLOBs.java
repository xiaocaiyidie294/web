package cn.czh.web.model;

import java.util.Date;

public class VisitLogWithBLOBs extends VisitLog {
    private String params;

    private String result;

    private String agent;

    public VisitLogWithBLOBs(Integer id, String url, String type, Short code, String ip, String username, Short spend, Date time, String params, String result, String agent) {
        super(id, url, type, code, ip, username, spend, time);
        this.params = params;
        this.result = result;
        this.agent = agent;
    }

    public VisitLogWithBLOBs() {
        super();
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }
}