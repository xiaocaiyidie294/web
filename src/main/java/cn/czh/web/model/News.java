package cn.czh.web.model;

import java.io.Serializable;

public class News implements Serializable {
    private Integer id;

    private String title;

    private String time;

    private String type;

    private String url;

    public News(Integer id, String title, String time, String type) {
        this.id = id;
        this.title = title;
        this.time = time;
        this.type = type;
    }

    public News(Integer id, String title, String time, String type, String url) {
        this.id = id;
        this.title = title;
        this.time = time;
        this.type = type;
        this.url = url;
    }

    public News() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}