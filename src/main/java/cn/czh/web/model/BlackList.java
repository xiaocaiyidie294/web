package cn.czh.web.model;

import java.util.Date;

public class BlackList {
    private String username;

    private Date time;

    private String reason;

    public BlackList(String username, Date time) {
        this.username = username;
        this.time = time;
    }

    public BlackList(String username, Date time, String reason) {
        this.username = username;
        this.time = time;
        this.reason = reason;
    }

    public BlackList() {
        super();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}