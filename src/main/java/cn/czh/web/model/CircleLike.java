package cn.czh.web.model;

import java.util.Date;

public class CircleLike {
    private Integer circleId;

    private String username;

    private Date time;

    public CircleLike(Integer circleId, String username, Date time) {
        this.circleId = circleId;
        this.username = username;
        this.time = time;
    }

    public CircleLike() {
        super();
    }

    public Integer getCircleId() {
        return circleId;
    }

    public void setCircleId(Integer circleId) {
        this.circleId = circleId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}