package cn.czh.web.model;

import java.util.Date;

public class User {
    private String username;

    private String password;

    private String name;

    private String sex;

    private String birthday;

    private String id;

    private String banji;

    private String tel;

    private String address;

    private String nickname;

    private Date createTime;

    private Date updateTime;

    private String remark;

    public User(String username, String password, String name, String sex, String birthday, String id, String banji, String tel, String address, String nickname, Date createTime, Date updateTime) {
        this.username = username;
        this.password = password;
        this.name = name;
        this.sex = sex;
        this.birthday = birthday;
        this.id = id;
        this.banji = banji;
        this.tel = tel;
        this.address = address;
        this.nickname = nickname;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    public User(String username, String password, String name, String sex, String birthday, String id, String banji, String tel, String address, String nickname, Date createTime, Date updateTime, String remark) {
        this.username = username;
        this.password = password;
        this.name = name;
        this.sex = sex;
        this.birthday = birthday;
        this.id = id;
        this.banji = banji;
        this.tel = tel;
        this.address = address;
        this.nickname = nickname;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.remark = remark;
    }

    public User() {
        super();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBanji() {
        return banji;
    }

    public void setBanji(String banji) {
        this.banji = banji;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", birthday='" + birthday + '\'' +
                ", id='" + id + '\'' +
                ", banji='" + banji + '\'' +
                ", tel='" + tel + '\'' +
                ", address='" + address + '\'' +
                ", nickname='" + nickname + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", remark='" + remark + '\'' +
                '}';
    }
}