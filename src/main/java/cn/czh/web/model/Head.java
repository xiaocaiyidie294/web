package cn.czh.web.model;

import java.io.Serializable;

public class Head {
    private String username;

    private String uuid;

    public Head(String username, String uuid) {
        this.username = username;
        this.uuid = uuid;
    }

    public Head() {
        super();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}