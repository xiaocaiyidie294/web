package cn.czh.web.model;

import java.util.Date;

public class CircleImage {
    private Integer circleId;

    private String uuid;

    private Date time;

    public CircleImage(Integer circleId, String uuid, Date time) {
        this.circleId = circleId;
        this.uuid = uuid;
        this.time = time;
    }

    public CircleImage() {
        super();
    }

    public Integer getCircleId() {
        return circleId;
    }

    public void setCircleId(Integer circleId) {
        this.circleId = circleId;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}