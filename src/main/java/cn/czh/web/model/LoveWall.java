package cn.czh.web.model;

import java.util.Date;

public class LoveWall {
    private Integer id;

    private String username;

    private String target;

    private Date time;

    private String text;

    public LoveWall(Integer id, String username, String target, Date time) {
        this.id = id;
        this.username = username;
        this.target = target;
        this.time = time;
    }

    public LoveWall(Integer id, String username, String target, Date time, String text) {
        this.id = id;
        this.username = username;
        this.target = target;
        this.time = time;
        this.text = text;
    }

    public LoveWall() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}