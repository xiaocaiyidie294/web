package cn.czh.web.model;

public class XueSheng {
    private Integer id;

    private String name;

    private String sex;

    private String xh;

    private String department;

    private String bj;

    public XueSheng(Integer id, String name, String sex, String xh, String department, String bj) {
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.xh = xh;
        this.department = department;
        this.bj = bj;
    }

    public XueSheng() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getXh() {
        return xh;
    }

    public void setXh(String xh) {
        this.xh = xh;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getBj() {
        return bj;
    }

    public void setBj(String bj) {
        this.bj = bj;
    }

}