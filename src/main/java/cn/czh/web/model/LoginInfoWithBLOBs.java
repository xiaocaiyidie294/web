package cn.czh.web.model;

import java.util.Date;

public class LoginInfoWithBLOBs extends LoginInfo {
    private String remark;

    private String agent;

    public LoginInfoWithBLOBs(String username, String password, Date time, Boolean status, String ip, String remark, String agent) {
        super(username, password, time, status, ip);
        this.remark = remark;
        this.agent = agent;
    }

    public LoginInfoWithBLOBs() {
        super();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }
}