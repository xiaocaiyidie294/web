package cn.czh.web.model;

import java.util.Date;

public class Circle {
    private Integer circleId;

    private String username;

    private Date time;

    private String content;

    public Circle(Integer circleId, String username, Date time) {
        this.circleId = circleId;
        this.username = username;
        this.time = time;
    }

    public Circle(Integer circleId, String username, Date time, String content) {
        this.circleId = circleId;
        this.username = username;
        this.time = time;
        this.content = content;
    }

    public Circle() {
        super();
    }

    public Integer getCircleId() {
        return circleId;
    }

    public void setCircleId(Integer circleId) {
        this.circleId = circleId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}