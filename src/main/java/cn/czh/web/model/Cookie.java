package cn.czh.web.model;

import java.util.Date;

public class Cookie {
    private String user;

    private String cookie;

    private Date time;

    public Cookie(String user, String cookie, Date time) {
        this.user = user;
        this.cookie = cookie;
        this.time = time;
    }

    public Cookie() {
        super();
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getCookie() {
        return cookie;
    }

    public void setCookie(String cookie) {
        this.cookie = cookie;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}