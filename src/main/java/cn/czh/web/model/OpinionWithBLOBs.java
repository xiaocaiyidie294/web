package cn.czh.web.model;

import java.util.Date;

public class OpinionWithBLOBs extends Opinion {
    private String text;

    private String uuids;

    public OpinionWithBLOBs(Integer id, String xh, Date time, String contact, Integer star, String text, String uuids) {
        super(id, xh, time, contact, star);
        this.text = text;
        this.uuids = uuids;
    }

    public OpinionWithBLOBs() {
        super();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUuids() {
        return uuids;
    }

    public void setUuids(String uuids) {
        this.uuids = uuids;
    }
}