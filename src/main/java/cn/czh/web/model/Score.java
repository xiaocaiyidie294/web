package cn.czh.web.model;

public class Score {
    private String sid;

    private String code;

    private String course;

    private String xq;

    private String psf;

    private String ksf;

    private String zf;

    private String remark;

    private String remark2;

    public Score(String sid, String code, String course, String xq, String psf, String ksf, String zf, String remark, String remark2) {
        this.sid = sid;
        this.code = code;
        this.course = course;
        this.xq = xq;
        this.psf = psf;
        this.ksf = ksf;
        this.zf = zf;
        this.remark = remark;
        this.remark2 = remark2;
    }

    public Score() {
        super();
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getXq() {
        return xq;
    }

    public void setXq(String xq) {
        this.xq = xq;
    }

    public String getPsf() {
        return psf;
    }

    public void setPsf(String psf) {
        this.psf = psf;
    }

    public String getKsf() {
        return ksf;
    }

    public void setKsf(String ksf) {
        this.ksf = ksf;
    }

    public String getZf() {
        return zf;
    }

    public void setZf(String zf) {
        this.zf = zf;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2;
    }
}