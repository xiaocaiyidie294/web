package cn.czh.web.model;

import java.util.Date;

public class Event {
    private Integer id;

    private String name;

    private Date time;

    private String user;

    private String detail;

    public Event(Integer id, String name, Date time, String user) {
        this.id = id;
        this.name = name;
        this.time = time;
        this.user = user;
    }

    public Event(Integer id, String name, Date time, String user, String detail) {
        this.id = id;
        this.name = name;
        this.time = time;
        this.user = user;
        this.detail = detail;
    }

    public Event() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}