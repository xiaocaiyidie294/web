package cn.czh.web.model;

import java.util.Date;

public class LoginInfo {
    private String username;

    private String password;

    private Date time;

    private Boolean status;

    private String ip;

    public LoginInfo(String username, String password, Date time, Boolean status, String ip) {
        this.username = username;
        this.password = password;
        this.time = time;
        this.status = status;
        this.ip = ip;
    }

    public LoginInfo() {
        super();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}