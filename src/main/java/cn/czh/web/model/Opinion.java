package cn.czh.web.model;

import java.util.Date;

public class Opinion {
    private Integer id;

    private String xh;

    private Date time;

    private String contact;

    private Integer star;

    public Opinion(Integer id, String xh, Date time, String contact, Integer star) {
        this.id = id;
        this.xh = xh;
        this.time = time;
        this.contact = contact;
        this.star = star;
    }

    public Opinion() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getXh() {
        return xh;
    }

    public void setXh(String xh) {
        this.xh = xh;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public Integer getStar() {
        return star;
    }

    public void setStar(Integer star) {
        this.star = star;
    }
}