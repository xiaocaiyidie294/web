package cn.czh.web.model;

import java.util.Date;

public class Course {
    private Integer id;

    private String banji;

    private String department;

    private Date start;

    private Date end;

    private String name;

    private String teacher;

    private String classroom;

    private String remark;

    private String group;

    private Integer week;

    public Course(Integer id, String banji, String department, Date start, Date end, String name, String teacher, String classroom, String remark, String group, Integer week) {
        this.id = id;
        this.banji = banji;
        this.department = department;
        this.start = start;
        this.end = end;
        this.name = name;
        this.teacher = teacher;
        this.classroom = classroom;
        this.remark = remark;
        this.group = group;
        this.week = week;
    }

    public Course() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBanji() {
        return banji;
    }

    public void setBanji(String banji) {
        this.banji = banji;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getClassroom() {
        return classroom;
    }

    public void setClassroom(String classroom) {
        this.classroom = classroom;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public Integer getWeek() {
        return week;
    }

    public void setWeek(Integer week) {
        this.week = week;
    }
}