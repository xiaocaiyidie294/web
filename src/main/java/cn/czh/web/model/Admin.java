package cn.czh.web.model;

public class Admin {
    private String username;

    private String password;

    private Short permission;

    public Admin(String username, String password, Short permission) {
        this.username = username;
        this.password = password;
        this.permission = permission;
    }

    public Admin() {
        super();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Short getPermission() {
        return permission;
    }

    public void setPermission(Short permission) {
        this.permission = permission;
    }
}