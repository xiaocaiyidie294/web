package cn.czh.web.model;

public class Jwtz {
    private Integer id;

    private String title;

    private String time;

    private String department;

    private String term;

    public Jwtz(Integer id, String title, String time, String department, String term) {
        this.id = id;
        this.title = title;
        this.time = time;
        this.department = department;
        this.term = term;
    }

    public Jwtz() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }
}