package cn.czh.web.task;

import cn.czh.web.dao.CircleImageMapper;
import cn.czh.web.dao.HeadMapper;
import cn.czh.web.web.COS;
import com.qcloud.cos.model.COSObjectSummary;
import com.qcloud.cos.model.ListObjectsRequest;
import com.qcloud.cos.model.ObjectListing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class COSTask {

    Logger logger = LoggerFactory.getLogger(COSTask.class);

    @Autowired
    CircleImageMapper imageMapper;
    @Autowired
    HeadMapper headMapper;

    //每天零时执行一遍
    @Scheduled(cron = "0 0 0 * * ?")
    public void start() {
        logger.info("腾讯云对象存储清理>>>>>>>>");

        ListObjectsRequest listObjectsRequest = new ListObjectsRequest();
        listObjectsRequest.setBucketName(COS.BUCKET_NAME);
        // 设置 list 的 prefix, 表示 list 出来的文件 key 都是以这个 prefix 开始
        listObjectsRequest.setPrefix("");
        // 设置 delimiter 为/, 即获取的是直接成员，不包含目录下的递归子成员
        listObjectsRequest.setDelimiter("/");
        // 设置 marker, (marker 由上一次 list 获取到, 或者第一次 list marker 为空)
        listObjectsRequest.setMarker("");
        // 设置最多 list 100 个成员,（如果不设置, 默认为 1000 个，最大允许一次 list 1000 个 key）
        listObjectsRequest.setMaxKeys(1000);

        boolean isTruncated = true;

        while (isTruncated){
            ObjectListing objectListing = COS.getClient().listObjects(listObjectsRequest);
            // 获取下次 list 的 marker
            String nextMarker = objectListing.getNextMarker();
            // 标记下次请求list 的 marker
            listObjectsRequest.setMarker(nextMarker);
            // 判断是否已经 list 完, 如果 list 结束, 则 isTruncated 为 false, 否则为 true
            isTruncated = objectListing.isTruncated();

            List<COSObjectSummary> objectSummaries = objectListing.getObjectSummaries();
            logger.info("文件列表>>>>>>>>>>" + objectSummaries.size());
            for (COSObjectSummary cosObjectSummary : objectSummaries) {
                // 文件路径
                String key = cosObjectSummary.getKey();
                // 获取文件长度
                long fileSize = cosObjectSummary.getSize();
                // 获取文件ETag
                String eTag = cosObjectSummary.getETag();
                // 获取最后修改时间
                Date lastModified = cosObjectSummary.getLastModified();
                // 获取文件的存储类型
                String StorageClassStr = cosObjectSummary.getStorageClass();

                //打印日志
                logger.info("文件路径:" + key);
                logger.info("文件长度:" + fileSize);
                logger.info("文件ETag:" + eTag);
                logger.info("最后修改时间:" + lastModified.toGMTString());
                logger.info("文件存储类型:" + StorageClassStr);


            }
        }
    }
}
