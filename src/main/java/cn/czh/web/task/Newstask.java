package cn.czh.web.task;

import cn.czh.web.service.NewsDemo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.Spider;

@Component
public class Newstask {

    Logger logger = LoggerFactory.getLogger(Newstask.class);

    @Autowired
    private NewsDemo demo;

    //每天凌晨六点执行爬取新闻动态任务
    @Scheduled(cron = "0 0 6 * * ?")
    public void start() {
        logger.info("新闻动态爬取任务>>>>>>>>");
        String[] website = new String[]{"http://www.jmpt.cn/viscms/jmpt/jiangzhiyaowen4687/" , "http://www.jmpt.cn/viscms/jmpt/xiaoyuanchuanzhen8245/",
                "http://clx.jmpt.cn/viscms/clx/xinwendongtai5741/","http://clx.jmpt.cn/viscms/clx/xinwendongtai5741/",
                "http://dzx.jmpt.cn/viscms/dzx/xinwenzhongxin2312/","http://dzx.jmpt.cn/viscms/dzx/tongzhigonggao9785/",
                "http://jgx.jmpt.cn/viscms/jgx/xinwensudi7805/","http://jdx.jmpt.cn/viscms/jdx/jidiandongtai4120/",
                "http://jdx.jmpt.cn/viscms/jdx/xuezifengcai5049/","http://jyx.jmpt.cn/viscms/jyx/zonghexinwen0566/",
                "http://wyx.jmpt.cn/viscms/wyx/xinwenzhongxin8566/","http://ysx.jmpt.cn/viscms/ysx/xibuxinwen5047/"};

        for(int i=0 ; i<website.length ; i++){
            Spider.create(demo).addUrl(website[i]).thread(5).run();
        }
    }

}
