package cn.czh.web.task;

import cn.czh.web.dao.CourseMapper;
import cn.czh.web.dao.UserMapper;
import cn.czh.web.model.Course;
import cn.czh.web.model.CourseExample;
import cn.czh.web.util.*;
import cn.czh.web.web.Application;
import com.alibaba.fastjson.JSONObject;
import okhttp3.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

@Component
public class CourseTask {

    private Logger logger= LoggerFactory.getLogger(CourseTask.class);

    @Autowired
    public CourseMapper courseMapper;

    /**
     * 每天凌晨四点执行爬取班级课程任务
     */
    @Scheduled(cron = "0 0 4 * * ?")
    public void start(){
        JSONObject loginResult = LoginUtil.login("teacher", "3268", "85421");
        if (loginResult.containsKey("error") || loginResult.containsKey("tip")) {
            logger.error("爬取课程任务失败：" + loginResult.getString("error") + "\t" + loginResult.getString("tip"));
            return;
        }

        String cookie = loginResult.getString("cookie");
        String xq;
        int nowWeek;
        List<String> list = new LinkedList<String>();

        try {
            String params = "pageId=000202&actionId=202";
            Response response = WebUtil.post(Configuration.commitUrl , cookie , params);
            Document document = Jsoup.parse(response.body().string());
            xq = document.getElementsByAttributeValue("name","Xq").attr("value");
            logger.info("xq:" + xq);
            Elements font = document.select("body > form > table > tbody > tr > td > table > tbody > tr:nth-child(2) > td:nth-child(3) > table > tbody > tr:nth-child(3) > td:nth-child(2) > font");
            logger.info(font.text());
            nowWeek = Integer.parseInt(TextUtil.findText(font.text() , "第" , "周"));
            Elements select = document.getElementsByAttributeValue("name","PkBj");
            Elements options = select.select("option");
            for(Element element : options){
                list.add(element.attr("value").concat(" ").concat(element.text()));
            }

        } catch (IOException e) {
            e.printStackTrace();
            logger.error("爬取课程任务失败：" + e.getMessage());
            return;
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.add(Calendar.WEEK_OF_YEAR , (nowWeek-1)*-1);
        calendar.set(Calendar.DAY_OF_WEEK , Calendar.MONDAY);
        calendar.set(Calendar.HOUR_OF_DAY , 0);
        calendar.set(Calendar.MINUTE , 0);
        calendar.set(Calendar.SECOND ,0);
        String[] weeks = {"星期一","星期二","星期三","星期四","星期五","星期六","星期日"};
        int[] startHours = {0,8,9,10,10,14,15,16,17,18,19,20,21};
        int[] startMinutes = {0,15,10,10,5,30,25,20,15,10,30,25,20};
        int[] endHours = {0,9,9,10,11,15,16,17,18,18,20,21,22};
        int[] endMinutes = {0,0,55,55,50,15,10,5,0,55,15,10,5};
        for(String s : list){
            try {
                String[] split = s.split(" ");
                logger.info("正在爬取班级课程：" +  split[2]);
                String params = "cPkXq=" + URLEncoder.encode(xq , "gbk") + "&" +
                        "cPkBj=" + split[0] + "&" +
                        "cPkZq=0&" +
                        "pageId=120201&" +
                        "actionId=register";
                Response response = WebUtil.post(Configuration.commitUrl , cookie , params);
                Document document = Jsoup.parse(response.body().string());
                Elements trs = document.select("body > form > table > tbody > tr > td > table > tbody > tr:nth-child(2) > td:nth-child(3) > table > tbody > tr:nth-child(4) > td > table > tbody").select("tr");
                CourseExample example = new CourseExample();
                example.createCriteria().andBanjiEqualTo(split[2]);
                courseMapper.deleteByExample(example);

                String week = "星期一";
                for(int i=1 ; i<trs.size()-2 ; i++){
                    Elements tds = trs.get(i).select("td");
                    int len = tds.size();
                    if(len == 12) week = tds.get(len-12).text();
                    String cycle = tds.get(len-11).text();
                    int start = Integer.parseInt(tds.get(len-10).text());
                    int end = Integer.parseInt(tds.get(len-9).text());
                    int startWeek = Integer.parseInt(TextUtil.findText(cycle , "" , "-"));
                    int endWeek = Integer.parseInt(TextUtil.findText(cycle , "-" , "周"));
                    String name = tds.get(len-8).text();
                    String teacher = tds.get(len-6).text();
                    String classroom = tds.get(len-5).text();
                    String remark = tds.get(len-4).text() + " " + tds.get(len-3).text();
                    String group = tds.get(len-2).text();

                    Calendar time = (Calendar) calendar.clone();
                    time.add(Calendar.DAY_OF_WEEK , ArrayUtil.indexOf(weeks , week));
                    for(int j=startWeek ; j<=endWeek ; j++){
                        Course course = new Course();

                        Calendar startTime = (Calendar) time.clone();
                        Calendar endTime = (Calendar) time.clone();
                        startTime.add(Calendar.WEEK_OF_YEAR , j-1);
                        startTime.add(Calendar.HOUR_OF_DAY , startHours[start]);
                        startTime.add(Calendar.MINUTE , startMinutes[start]);
                        endTime.add(Calendar.WEEK_OF_YEAR , j-1);
                        endTime.add(Calendar.HOUR_OF_DAY , endHours[end]);
                        endTime.add(Calendar.MINUTE , endMinutes[end]);
                        course.setStart(startTime.getTime());
                        course.setEnd(endTime.getTime());

                        course.setBanji(split[2]);
                        course.setDepartment(split[3]);
                        course.setName(name);
                        course.setTeacher(teacher);
                        course.setClassroom(classroom);
                        course.setRemark(remark);
                        course.setGroup(group);
                        course.setWeek(j);

                        courseMapper.insert(course);
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }


}
