package cn.czh.web.web;

import okhttp3.OkHttpClient;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Application {
//    static ApplicationContext context = new ClassPathXmlApplicationContext("spring/spring-mybatis.xml");
    static Application context;
    static OkHttpClient httpClient = null;

    public static OkHttpClient getHttpClient(){
        if(httpClient==null){
            httpClient = new OkHttpClient.Builder()
//                    .proxy(new Proxy(Proxy.Type.HTTP , new InetSocketAddress("127.0.0.1" , 8888)))
                    .build();
        }
        return httpClient;
    }
}
