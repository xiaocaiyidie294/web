package cn.czh.web.web;

import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.region.Region;

public class COS {
    // bucket的命名规则为{name}-{appid} ，此处填写的存储桶名称必须为此格式
    public static String BUCKET_NAME = "images-1255609946";
    public static final String IMAGE_URL = "https://images-1255609946.picgz.myqcloud.com/";
    public static final String DEFAULT_HEAD_UUID = "acaaa45a-9c3a-4bf6-a0aa-5173982d84d9";
    public static COSClient client;

    public static COSClient getClient() {
        if(client==null){
            COSCredentials cred = new BasicCOSCredentials("AKIDfHl7MUSN8iAR8froU2RirriR2UlyHtZ6", "Wr10xmS0QittjOr9HE4lIepphYHrNm3C");
            ClientConfig clientConfig = new ClientConfig(new Region("ap-guangzhou"));
            client = new COSClient(cred, clientConfig);
        }

        return client;
    }

    public static String getImageUrl(String uuid){
        return IMAGE_URL + uuid;
    }

    public static String getNormalJpg(String url){
        return url + "!normal+jpg";
    }

    public static String getNormalWebp(String url){
        return url + "!normal+webp";
    }

    public static String getSmallJpg(String url){
        return url + "!small+jpg";
    }

    public static String getSmallWebp(String url){
        return url + "!small+webp";
    }

    public static String getMedianJpg(String url){
        return url + "!median+jpg";
    }

    public static String getMedianWebp(String url){
        return url + "!median+webp";
    }

    public static String getHeadJpg(String url){
        return url + "!head+jpg";
    }

    public static String getHeadWebp(String url){
        return url + "!head+webp";
    }

}
